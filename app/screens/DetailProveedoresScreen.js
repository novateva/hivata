import React from 'react';
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, ActivityIndicator, Image} from 'react-native';
import StarRating from 'react-native-star-rating';
import Categories from '../services/categories';

export default class ProveedorScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      starCount: 3.5,
      events:[]
    };
    Categories.profileEvent(this.props.navigation.state.params.categories_id)
            .then(data=>this.setState({events:data[0].profiles ? data[0].profiles : [], activity:true}))

  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
  }

  render() {
    
    return (
      this.state.activity ?
        (!this.state.events.length 
          ?
          <View style={{width:"100%", height:'100%', flexDirection:'column', alignItems: 'center'}}>
            <Image
              style={styles.imagen}
              source={require('../img/triste.png')}
            />
            <Text style={{textAlign:'center', fontSize:20, fontWeight:'bold'}} >{'¡No hay proveedores!'}</Text>
            <Text style={{textAlign:'center'}} >{'¡Oh no! no hay proveedores de esta categoria!'}</Text>
            <TouchableOpacity 
              style={styles.button}
              onPress={()=>this.props.navigation.navigate('EventoCreate')}>
              {
  
                <Text style={styles.buttonText}>Nuevo</Text>  
              }
            </TouchableOpacity>
              
          </View>
          :
          <ScrollView style={styles.contenedor}>
          
            {this.state.events.map(data=>(
                <TouchableOpacity onPress={()=>this.props.navigation.navigate('DetailPerfil',{...data})} style={styles.proveedor}>
                  <Text style={styles.proveedorNombre}>{data.name}</Text>
                  <Text style={styles.proveedorDescripcion}>{data.description === 'null' ? '' : data.description}</Text>
                  <StarRating
                    disabled={true}
                    maxStars={5}
                    rating={data.rating/data.finishWork}
                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                    fullStarColor={'#f1c40f'}
                    starSize={20}
                    containerStyle={{width: 150, marginTop:10}}
                  />
                </TouchableOpacity>
              ))}

          </ScrollView>
        )
        :<View style={[styles.contenedor, {marginTop:'20%'}]}>
          <ActivityIndicator size="large" color="#ea1d75" />
        </View>

    );
  }
}

const styles = StyleSheet.create({
  proveedor : {
    padding: 20,
    borderBottomColor: "#ecf0f1",
    borderBottomWidth: 1,
    backgroundColor: "#fff",
  },

  proveedorNombre : {
    fontSize: 17,
    marginBottom: 5,
    fontWeight: 'normal',
    color: "#DEB749"
  },

  imagen:{
    width:30, 
    height:30, 
    marginTop:'48%'
  }

});


