import React from 'react';
import { WebView, Text, View, StyleSheet, Image, TextInput, TouchableOpacity, StatusBar, ScrollView, ActivityIndicator, Modal, Linking, Dimensions} from 'react-native';
import SegmentedControlTab from 'react-native-segmented-control-tab';
import RadioButton from 'react-native-radio-button'
import User from '../services/user';
import ERROR from '../config/Error';
import { Permissions, Notifications } from 'expo';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Select from 'react-native-picker-select';
import Firebase from '../config/firebase';
import CheckBox from '../components/Checkbox';

const width = Dimensions.get('window').width;
export default class LoginScreen extends React.Component {
	static navigationOptions = {
		headerTintColor:"#fff",
		headerStyle: {
				backgroundColor: '#000',
				borderColor: "#000",
		},
		header:null,
	}

	constructor(){
		super()
		this.state = {
			roles_id: 2,
			name:'',
			email:'',
			password:'',
			repetePassword:'',
			city:'',
			date:'',
			activity:false,
			isModal:false,
			payment:0,
			description:'',
			token:'a',
			genre:'',
			paypalModal:false
		};
		this.register = this.register.bind(this);
		this.registerForPushNotificationsAsync();
		this.items = [
			"Aguascalientes",
 			"Baja California",
  			"Baja California Sur",
  			"Campeche",
  			"Chiapas",
  			"Chihuahua",
  			"Ciudad de México",
  			"Coahuila de Zaragoza",
  			"Colima",
  			"Durango",
  			"Estado de México",
  			"Guanajuato",
  			"Guerrero",
  			"Hidalgo",
  			"Jalisco",
  			"Michoacán de Ocampo",
  			"Morelos",
  			"Nayarit",
  			"Nuevo León",
  			"Oaxaca",
  			"Puebla",
  			"Querétaro",
  			"Quintana Roo",
  			"San Luis Potosí",
  			"Sinaloa",
  			"Sonora",
  			"Tabasco",
  			"Tamaulipas",
  			"Tlaxcala",
  			"Veracruz de Ignacio de la Llave",
  			"Yucatán",
  			"Zacatecas",
		]
	}

	handleIndexChange = (index) => {
		this.setState({
			roles_id: index ? 1 : 2,
		});
	}

	onChangeValue(name, value){
		this.setState({[name]:value})
	}

	async registerForPushNotificationsAsync() {
		const { status: existingStatus } = await Permissions.getAsync(
			Permissions.NOTIFICATIONS
		);
		let finalStatus = existingStatus;

		if (existingStatus !== 'granted') {
			const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
			finalStatus = status;
		}

		if (finalStatus !== 'granted') {
			return;
		}
		this.state.token = await Notifications.getExpoPushTokenAsync();

	}

	register(){
		//validacion
		let bandera = false;
		let errores = {errores:{errEmail:'', errPassword:''}}

		if (this.state.name === "") {
			bandera = true;
			ERROR("Debe introducir un nombre","Error");
			return;
		}

		if (this.state.email === "") {
			bandera = true;
			ERROR("Debe introducir un correo","Error");
			return;
		}

		if (this.state.password === "") {
			bandera = true;
			ERROR("Debe introducir una contraseña", "Error");
			return;
		}

		if (this.state.repetePassword === "") {
			bandera = true;
			ERROR("Repita la contraseña", "Error");
			return;
		} else if (this.state.password !== this.state.repetePassword) {
			bandera = true;
			ERROR("Contraseñas no coinciden", "Error");
			return;
		}

		if (this.state.city === "") {
			bandera = true;
			ERROR("Debe introducir una ciudad", "Error");
			return;
		}

		if(this.state.dia === ""){
			bandera = true;
			ERROR("Debe introducir un día","Error");
			return;
		}
		if(this.state.mes === "" ){
			bandera = true;
			ERROR("Debe introducir un mes","Error");
			return;
		}
		if( this.state.anio === ""){
			bandera = true;
			ERROR("Debe introducir un año","Error");
			return;
		}

		if( this.state.genre === ""){
			bandera = true;
			ERROR("Debe introducir un genero","Error");
			return;
		}
		if (!this.state.terms) {
			bandera = true;
			ERROR("Debe aceptar términos y condiciones","Error");
			return;
		}
		if (!bandera) {
			this.state.date = this.state.anio + '/' + this.state.mes + '/' + this.state.dia
			this.setState({activity:true})


			User.register(this.state)
			.then(
				(dataToken) => {

					if(!dataToken.error){
						ERROR('Registro exitoso!! valida tu correo', 'Éxito');
						this.props.navigation.navigate('Login');
					}else {
						ERROR("Credenciales inválidas", "Error");


					}
					this.setState({activity:false})
				}
			).catch(error => {
				ERROR("Correo esta ya registrado", "Error");
						this.setState({activity:false})
			})
		}
	}
	openPaypalModal(){
		this.state.code = this.convertToHex(JSON.stringify(this.state))
		//
		this.setState({loadingPaypal:true});
		Firebase.database().ref(`updateuser`).on('value', snapshot=>{

				if (snapshot.val()) {
					if (snapshot.val()[this.state.code]) {
					  if(snapshot.val()[this.state.code].payment.value){
						  this.setState({paypalModal:false, payment:1})
						  ERROR("Pago registrado", "Éxito");
						} else {
							this.setState({paypalModal:false, payment:0})
							ERROR("Pago cancelado", "Error");
						}
					}

					Firebase.database().ref(`updateuser/${this.state.code}/payment`).set(null)
				}


			})
		User.getSuscriptionUrl(this.state.code)
			.then((data)=>{

			  this.setState({urlPaypal:data.url, paypalModal:true, loadingPaypal:false, isModal:false})
			 //


			})
	}

	convertFromHex(hex) {
		var hex = hex.toString();//force conversion
		var str = '';
		for (var i = 0; i < hex.length; i += 2)
			str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
		return str;
	}

	convertToHex(str) {
		var hex = '';
		for(var i=0;i<str.length;i++) {
			hex += ''+str.charCodeAt(i).toString(16);
		}
		return hex.substr(0, 250);
	}
	render() {
		console.log(width)
		return (
			<View style={styles.container}>
				<StatusBar barStyle = "light-content" backgroundColor="#000" />
				<ScrollView contentContainerstyle={[styles.container, {width:'100%',alignItems: "center", justifyContent: "center",}]}>
					<View style={{justifyContent:'center', alignItems:'center'}}>
						<Image
							style={styles.logo}
							source={require('../img/logo.png')}
						/>
					</View>

					<TextInput
						style={styles.input}
						placeholder="Nombres"
						placeholderTextColor='rgba(255,255,255, 0.7)'
						underlineColorAndroid={'rgba(0,0,0,0)'}
						ref={component => this._name = component}
          				onSubmitEditing={()=>this._email.focus()}
						onChangeText={(text)=>this.onChangeValue('name', text)}
						>
					</TextInput>

					<TextInput
						style={styles.input}
						placeholder="Correo Electrónico"
						ref={component => this._email = component}
          				onSubmitEditing={()=>this._password.focus()}
						placeholderTextColor= 'rgba(255,255,255, 0.7)'
						onChangeText={(text)=>this.onChangeValue('email', text)}
						underlineColorAndroid={'rgba(0,0,0,0)'}>
					</TextInput>

					 <TextInput
						style={styles.input}
						placeholder="Contraseña"
						secureTextEntry
						ref={component => this._password = component}
          				onSubmitEditing={()=>this._repetepassword.focus()}
						placeholderTextColor= 'rgba(255,255,255, 0.7)'
						onChangeText={(text)=>this.onChangeValue('password', text)}
						underlineColorAndroid={'rgba(0,0,0,0)'}>
					</TextInput>

					<TextInput
						style={styles.input}
						placeholder="Confirmar Contraseña"
						secureTextEntry
						ref={component => this._repetepassword = component}

						placeholderTextColor= 'rgba(255,255,255, 0.7)'
						onChangeText={(text)=>this.onChangeValue('repetePassword', text)}
						underlineColorAndroid={'rgba(0,0,0,0)'}>
					</TextInput>



					<View style={{backgroundColor:'rgba(255, 255, 255, 0.3)',borderRadius:3, marginBottom:20}}>
						<Select
		                    placeholder={{
		                        label: 'Selecciona estado...',
		                        value: '',
		                    }}
		                    items={this.items.map(data=>({label:data, value:data}))}
		                    onValueChange={(value) => this.onChangeValue('city', value)}
		                    value={this.state.city}
		                    style={{
								padding: 10,
								backgroundColor:'rgba(255, 255, 255, 0.3)',
								height: 50,
								inputAndroid:{
									borderRadius:3,
									color:'#fff'
								},
								alignSelf: 'stretch',
								color:"#fff",
								fontSize: 17,
								marginBottom: 20,
								borderRadius: 3,
								width:'100%',
								underline:{
									borderTopColor:'transparent'
								}
							}}
		                />
					</View>

					<View style={{backgroundColor:'rgba(255, 255, 255, 0.3)', borderRadius:3, marginBottom:20}}>
						<Select
		                    placeholder={{
		                        label: 'Selecciona tu genero...',
		                        value: '',
		                    }}
		                    items={[{label:'Masculino', value:'Masculino'},{label:'Femenino', value:'Femenino'}]}
		                    onValueChange={(value) => this.onChangeValue('genre', value)}
		                    value={this.state.genre}
		                    style={{
								padding: 10,
								backgroundColor:'rgba(255, 255, 255, 0.3)',
								height: 50,
								inputAndroid:{
									borderRadius:3,
									color:'#fff'
								},
								alignSelf: 'stretch',
								color:"#fff",
								fontSize: 17,
								marginBottom: 20,
								borderRadius: 3,
								width:'100%',
								underline:{
									borderTopColor:'transparent'
								}
							}}
		                />
					</View>



				 <View style={{'flexDirection':'row'}}>
					<TextInput
						style={[styles.input, {'width':'31%'}]}
						placeholder="DD"
						keyboardType='numeric'
						maxLength={2}
						ref={component => this._date = component}
          				onSubmitEditing={()=>this._month.focus()}
						onChangeText={(text)=>this.onChangeValue('dia', text)}
						placeholderTextColor= 'rgba(255,255,255, 0.7)'
						underlineColorAndroid={'rgba(0,0,0,0)'}>
					</TextInput>
					<TextInput
						style={[styles.input, {'marginLeft':'2%', 'width':'31%'}]}
						placeholder="MM"
						keyboardType='numeric'
						maxLength={2}
						ref={component => this._month = component}
          				onSubmitEditing={()=>this._year.focus()}
						onChangeText={(text)=>this.onChangeValue('mes', text)}
						placeholderTextColor= 'rgba(255,255,255, 0.7)'
						underlineColorAndroid={'rgba(0,0,0,0)'}>
					</TextInput>
					<TextInput
						style={[styles.input, {'marginLeft':'2%', 'width':'31%'}]}
						placeholder="AAAA"
						keyboardType='numeric'
						maxLength={4}
						ref={component => this._year = component}
          				onSubmitEditing={()=>{}}
						onChangeText={(text)=>this.onChangeValue('anio', text)}
						placeholderTextColor= 'rgba(255,255,255, 0.7)'
						underlineColorAndroid={'rgba(0,0,0,0)'}>
					</TextInput>
				</View>

				{/*this.state.roles_id === 1*/false ?
				<View>
					<Text style={[styles.label, {fontWeight:'bold'}]}>Destacar publicaciones</Text>
					<View style={{'flexDirection':'row'}}>
						<RadioButton
							animation={'bounceIn'}
							innerColor={'#ea1d75'}
							outerColor={'#ea1d75'}
							isSelected={this.state.payment}
							onPress={() =>{if(this.state.payment)return; this.setState({isModal:true})}}
						/>
						<Text style={[styles.label,{marginLeft:5}]} >
						 {"Suscripción Premium"}
						</Text>
					</View>
				</View>
				: null}

					<CheckBox
					    style={{ paddingBottom: 10 }}
					    onClick={()=>{
					      this.setState({
					          terms:!this.state.terms
					      })

					    }}
					    isChecked={this.state.terms}
					    checkBoxColor='rgba(255,255,255, 0.7)'
					    rightTextView={width > 360 ? <TouchableOpacity onPress={()=>{ Linking.openURL('http://198.211.114.51/Condicionesdelservicio.pdf')}}><Text style={{marginLeft:5 ,color:'rgba(255,255,255, 0.7)'}} >{"Acepta las condiciones y términos de servicios."}</Text></TouchableOpacity> : <TouchableOpacity onPress={()=>{ Linking.openURL('http://198.211.114.51/Condicionesdelservicio.pdf')}}><Text style={{marginLeft:5 ,color:'rgba(255,255,255, 0.7)'}} >{"Acepta las condiciones "}</Text><Text style={{marginLeft:5 ,color:'rgba(255,255,255, 0.7)'}} >{"y términos de servicios."}</Text></TouchableOpacity>}

					/>

					<SegmentedControlTab
							values={['Cliente', 'Proveedores']}
							selectedIndex={this.state.roles_id === 1 ? 1 : 0}
							onTabPress={this.handleIndexChange}
							tabStyle={{borderColor:'transparent',marginTop:10 , marginBottom:20, height: 50}}
							tabTextStyle={{color: "#666"}}
							activeTabStyle={{backgroundColor:"#feda73"}}
							activeTabTextStyle={{color: "#000"}}
					/>


					<TouchableOpacity
						style={styles.button}
						onPress={this.register}
						>
						{!this.state.activity ?
							<Text style={styles.buttonText}>Registrarse</Text>
							:
							<ActivityIndicator size="small" color="#fff" />
						}

					</TouchableOpacity>

					<TouchableOpacity
						style={styles.link}
						onPress={() => this.props.navigation.navigate('Login')}
						>
						<Text style={styles.buttonText}>Iniciar Sesión</Text>
					</TouchableOpacity>

					<Modal
						animationType="slide"
						transparent={true}
						visible={this.state.isModal}

						onRequestClose={() => {
							alert('Modal has been closed.');
						}}>
						<View style={{backgroundColor:'rgba(0,0,0,0.6)', width:'100%', height:'100%'}}>
							<View style={styles.modal}>
								<TouchableOpacity
										style={{position:'absolute', top:5,left:5}}
										onPress={()=>this.setState({isModal:false, payment:0})}
										>
									<MaterialIcons size={30} name='clear' />
								</TouchableOpacity>
								<View style={{flex:0.4}}>
									<Text style={[styles.modalText, {marginTop:'10%'}]}>{"Pagar via paypal"}</Text>
								</View>
								<View style={{flex:0.3, justifyContent:'center', alignItems:'center'}}>
									<Image
										style={styles.paypalLogo}
										source={require('../img/PayPal-simbolo.png')}
									/>
								</View>
								<View style={{flex:0.3}}>
									<Text style={styles.modalText}>{"10 USD mensual"}</Text>
								</View>
								<View style={{flex:0.05}}>
									<TouchableOpacity
										style={styles.buttonModal}
										onPress={()=>this.openPaypalModal()}
										>
										{
											this.state.loadingPaypal ?
											<ActivityIndicator color="#fff"/>
											:
											<Text style={styles.buttonText}>{"Pagar"}</Text>
										}


									</TouchableOpacity>
								</View>

							</View>
						</View>
					</Modal>
					<Modal
						animationType="slide"
						transparent={true}
						visible={this.state.paypalModal}

						onRequestClose={() => {
							this.setState({paypalModal:false})
						}}>
						<View style={{backgroundColor:'rgba(0,0,0,0.6)', width:'100%', height:'100%'}}>
							<WebView source={{uri: this.state.urlPaypal}}/>
						</View>
					</Modal>
				</ScrollView>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#000",
		padding: 20,
	},

	input: {
		padding: 10,
		backgroundColor:'rgba(255, 255, 255, 0.3)',
		height: 50,
		alignSelf: 'stretch',
		color:"#fff",
		fontSize: 17,
		marginBottom: 20,
		borderRadius: 3,
		width:'100%'
	},

	label : {
			fontSize: 17,
			color: "gray",
			marginBottom: 3
		},

	logo : {
		width: 100,
		resizeMode: 'contain',
		height: 100,
		marginBottom:20,

	},

	button : {
		height: 50,
		backgroundColor:"#ea1d75",
		// backgroundColor:"#feda73",
		alignSelf: 'stretch',
		paddingVertical: 15,
		borderRadius: 3,
	},
	buttonModal : {
		height: 50,
		backgroundColor:"#ea1d75",
		width:'80%',
		alignSelf: 'stretch',
		paddingVertical: 15,
		marginLeft:'10%',
		borderRadius: 3,
	},
	buttonText : {
		color: "#fff",
		textAlign:'center',
		fontSize: 17,
	},

	link : {
		marginTop: 30,
	},

	modal:{
		backgroundColor:'#eee',
		width:'60%',
		marginLeft:'20%',
		height:'40%',
		marginTop:'30%',
		borderColor:'black',
		borderWidth:1,
		borderRadius:10,
		justifyContent:'center'

	},
	modalText:{
		color: "#000",
		textAlign:'center',
		fontSize: 17,
	},

	paypalLogo:{
		width:30,
		height:30
	}




});
