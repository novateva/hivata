import React from 'react';
import { Button, View, StyleSheet, Image, TextInput, TouchableOpacity, ScrollView, Text, Modal, WebView, ActivityIndicator} from 'react-native';
import StarRating from 'react-native-star-rating';
import Proposal from '../services/proposal';
import Event from '../services/events';
import Ionicons from "react-native-vector-icons/Ionicons";
import User from '../services/user';
import Firebase from '../config/firebase';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import ERROR from '../config/Error';
import { EventRegister } from 'react-native-event-listeners';
import { NavigationActions } from 'react-navigation';

export default class EventoDetailScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          starCount: 0,
          proposal:[],
          isAcepted:false,
          proposalAcepted:{},
          isModal:false,
          isModalPay:false,
          paypalModal:false,
          deleteEvent:false,
        };
        this.getData = this.getData.bind(this);
        this.getData();


        this.acceptProposal = this.acceptProposal.bind(this);
        this.deleteEvent = this.deleteEvent.bind(this);
        this.onDeleteEvent = this.onDeleteEvent.bind(this);
        this.onUpdateEvent = this.onUpdateEvent.bind(this);
    }

    componentDidMount() {
        EventRegister.addEventListener('onDeleteEvent',this.onDeleteEvent);
        EventRegister.addEventListener('onUpdateEvent',this.onUpdateEvent);
    }
    getData () {
        Proposal.proposalEvent(this.props.navigation.state.params.event.id)
            .then(data=>{

                if(!data)return;

                for (var i = 0; i < data.length; i++) {
                    if (data[i].status > 0){
                        return this.setState({isAcepted:true, proposalAcepted:data[i]})
                    }
                }

                this.setState({proposal:data, isAcepted:false})
            });

    }

    onStarRatingPress(rating) {
        this.setState({
          rating
        });
    }

    acceptProposal(proposal) {
        
        Proposal.updateStatus(1, proposal.users_id, this.props.navigation.state.params.event.id);
        proposal.status = 1;
        this.setState({proposalAcepted:proposal, isAcepted:true}, ()=>{
           this.sendMesajeEvent();
        });
        
    }

    deleteEvent() {
        this.setState({loadingDelete:true})
        Event.Delete(this.props.navigation.state.params.event.id)
        .then(data=>{
          
          EventRegister.emit('eventCreated','event');
          ERROR("Evento Eliminado con éxito","éxito");
          this.props.navigation.goBack();
        })
        .catch((err)=>{
          console.log(err)
          this.setState({loadingDelete:false})
        })
    }

    onDeleteEvent() {
        this.setState({deleteEvent:true})
    }

    onUpdateEvent() {
        this.props.navigation.navigate('EventoEdit',{...this.props.navigation.state.params})
    }



    makePayment(){
        Proposal.updateStatus(3, this.state.proposalAcepted.users_id, this.props.navigation.state.params.event.id);
        this.setState({proposalAcepted:{...this.state.proposalAcepted, status:3}, isModalPay:false});
        ERROR("Evento finalizado pago registrado con éxito","éxito");
        EventRegister.emit('eventCreated','event');
        this.props.navigation.goBack();
    }

    finishWork(){
        if(this.state.rating === undefined) {
            ERROR("Califica al proveedor","Error");
            return;

        }
        Proposal.updateStatus(2, this.state.proposalAcepted.users_id, this.props.navigation.state.params.event.id, this.state.rating);
        this.setState({proposalAcepted:{...this.state.proposalAcepted, status:2}, isModal:false});
        if(!this.state.isModalPay){
            this.props.navigation.goBack();
        }
        ERROR("Evento finalizado con éxito","éxito");
        EventRegister.emit('eventCreated','event');

    }

    cancelPropousal() {
        Proposal.updateStatus(0, this.state.proposalAcepted.users_id, this.props.navigation.state.params.event.id, this.state.rating).then(data=>{
            this.getData();
        });

    }

    openPaypalModal(){
		this.state.code = this.convertToHex(JSON.stringify(this.state))
		//console.log('code', this.state.code)
		this.setState({loadingPaypal:true});
		Firebase.database().ref(`updateuser`).on('value', snapshot=>{
				console.log('val',snapshot.val())
				if (snapshot.val()) {
					if (snapshot.val()[this.state.code]) {
					  if(snapshot.val()[this.state.code].payment.value){
						  this.setState({paypalModal:false, loadingWeb:false })
						  this.makePayment();

						} else {
							this.setState({paypalModal:false, loadingWeb:false})
							ERROR("Pago cancelado", "Error");
						}
					}
					Firebase.database().ref(`updateuser/${this.state.code}/payment`).set(null)
				}


			})
		User.makeTrasaction(this.props.screenProps.user.token, this.state.proposalAcepted.users_id, this.state.code, this.state.proposalAcepted.budget)
			.then((data)=>{
			  
			  this.setState({urlPaypal:data.url, paypalModal:true, loadingPaypal:false, isModal:false, isModalPay:false})

			})
	}

	convertFromHex(hex) {
		var hex = hex.toString();//force conversion
		var str = '';
		for (var i = 0; i < hex.length; i += 2)
			str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
		return str;
	}

	convertToHex(str) {
		var hex = '';
		for(var i=0;i<str.length;i++) {
			hex += ''+str.charCodeAt(i).toString(16);
		}
		return hex.substr(0,250);
    }

   


    Direct(proposal) {
        Firebase.database().ref(`chat/${this.props.screenProps.user.user.id}/${'direct'+this.props.screenProps.user.user.id + proposal.users_id}`).once('value').then(data=>{
            ERROR("Chat creado con éxito", 'éxito');
            if (data.val()) {
                
                this.props.navigation.navigate('DetailMensaje', {idEvent:'direct'+this.props.screenProps.user.user.id + proposal.users_id});
            } else {
                Firebase.database().ref(`chat/${this.props.screenProps.user.user.id}/${'direct'+this.props.screenProps.user.user.id + proposal.users_id}`).set({proposal, event:this.props.navigation.state.params.event ,eventOwnerId:this.props.screenProps.user.user.id, proposalOnwerId: proposal.users_id, message:[]});
                const users = proposal.users;
                proposal.users = {...this.props.screenProps.user.user, profile:this.props.screenProps.user.profile}
                Firebase.database().ref(`chat/${proposal.users_id}/${'direct'+this.props.screenProps.user.user.id + proposal.users_id}`).set({proposal, event:this.props.navigation.state.params.event ,eventOwnerId:this.props.screenProps.user.user.id, proposalOnwerId: proposal.users_id, message:[]});
                proposal.users = users;   
                this.props.navigation.navigate('DetailMensaje', {idEvent:'direct'+this.props.screenProps.user.user.id + proposal.users_id});
            }
        })              
    }

    sendMesajeEvent(goToChat) {

        Firebase.database().ref(`chat/${this.props.screenProps.user.user.id}/${this.props.navigation.state.params.event.id}`).once('value').then(data=>{
            if (!data.val()) {
                Firebase.database().ref(`chat/${this.props.screenProps.user.user.id}/${this.props.navigation.state.params.event.id}`).set({proposal:this.state.proposalAcepted, event:this.props.navigation.state.params.event ,eventOwnerId:this.props.screenProps.user.user.id, proposalOnwerId: this.state.proposalAcepted.users_id, message:[]});
                const users = this.state.proposalAcepted.users;
                this.state.proposalAcepted.users = {...this.props.screenProps.user.user, profile:this.props.screenProps.user.profile}
                Firebase.database().ref(`chat/${this.state.proposalAcepted.users_id}/${this.props.navigation.state.params.event.id}`).set({proposal:this.state.proposalAcepted, event:this.props.navigation.state.params.event ,eventOwnerId:this.props.screenProps.user.user.id, proposalOnwerId: this.state.proposalAcepted.users_id, message:[]});
                this.state.proposalAcepted.users = users;
            }

            if (goToChat) {
                this.props.navigation.navigate('DetailMensaje', {idEvent:this.props.navigation.state.params.event.id});
            }
        })
        
    }

    render() {

        console.log(this.props.screenProps.user.user.roles_id )
        return (
            
          <ScrollView>
              <View style={styles.container}>
                <View style={styles.evento}>
                    <Text style={styles.eventoTitulo}>{this.props.navigation.state.params.event.name}</Text>
                    <Text style={styles.eventoDescripcion}>{this.props.navigation.state.params.event.description}</Text>
                    <Text style={styles.eventoFecha}>{`${this.props.navigation.state.params.event.date.split(' ')[0]} ${this.props.navigation.state.params.event.city}`}</Text>
                    <Text style={styles.eventoDescripcion}>{this.props.navigation.state.params.event.location}</Text>
                </View>

                <Text style={styles.title}>{ this.state.isAcepted ? (this.state.proposalAcepted.status < 2 ? "En curso" : "Finalizada") : "Propuestas"}</Text>
                    {
                        !this.state.isAcepted ?
                        this.state.proposal.map(data=>(
                            <View style={styles.proveedor}>
                                <View style={styles.proveedorNombrePrecio}>
                                    <TouchableOpacity onPress={()=>{if(this.props.screenProps.user.user.roles_id === 1) return;this.props.navigation.navigate('DetailPerfil',{user:data.users})}}><Text style={styles.proveedorNombre}>{data.users.profile.name}</Text></TouchableOpacity>
                                    <Text style={styles.proveedorPrecio}>{"$"+data.budget}</Text>
                                </View>
                                <View style={styles.proveedorAction}>
                                    <Text style={styles.proveedorDescripcion}>{data.description}</Text>
                                    <TouchableOpacity
                                        style={styles.button}
                                        onPress={()=>this.Direct(data)}
                                        >
                                        <Text style={styles.buttonText}>Enviar mensaje</Text>
                                    </TouchableOpacity>
                                </View>

                                <View style={styles.proveedorAction}>
                                    <StarRating
                                        disabled={true}
                                        maxStars={5}
                                        rating={data.users.profile.rating/data.users.profile.finishWork}
                                        selectedStar={(rating) => this.onStarRatingPress(rating)}
                                        fullStarColor={'#f1c40f'}
                                        starSize={20}
                                        containerStyle={{width: 150, marginTop:10}}
                                    />
                                    <TouchableOpacity
                                        style={styles.button}
                                        onPress={()=>this.acceptProposal(data)}
                                        >
                                        <Text style={styles.buttonText}>Aceptar Propuesta</Text>
                                    </TouchableOpacity>
                                </View>

                            </View>
                        ))
                        : <View style={styles.proveedor}>
                                <View style={styles.proveedorNombrePrecio}>
                                    <TouchableOpacity onPress={()=>{if(this.props.screenProps.user.user.roles_id === 1) return;this.props.navigation.navigate('DetailPerfil',{user:this.state.proposalAcepted.users})}}>
                                        <Text style={styles.proveedorNombre}>{this.state.proposalAcepted.users.profile.name}</Text>
                                    </TouchableOpacity>
                                    <Text style={styles.proveedorPrecio}>{"$"+this.state.proposalAcepted.budget}</Text>
                                </View>

                                <Text style={styles.proveedorDescripcion}>{this.state.proposalAcepted.description}</Text>
                                <View style={styles.proveedorAction}>
                                    <StarRating
                                        disabled={true}
                                        maxStars={5}
                                        rating={this.state.proposalAcepted.users.profile.rating/this.state.proposalAcepted.users.profile.finishWork   }
                                        selectedStar={(rating) => this.onStarRatingPress(rating)}
                                        fullStarColor={'#f1c40f'}
                                        starSize={20}
                                        containerStyle={{width: 150, marginTop:10}}
                                    />
                                    {this.props.screenProps.user.user.roles_id === 2 && <TouchableOpacity
                                        style={styles.button}
                                        onPress={()=>this.sendMesajeEvent(true)}
                                        >
                                        <Text style={styles.buttonText}>{"Enviar mensaje"}</Text>
                                    </TouchableOpacity>}
                                </View>

                        </View>

                    }
                    {
                        (this.state.isAcepted && !this.props.navigation.state.params.fromHistorial) && <View style={{marginTop:'5%'}}>
                            {/*this.state.proposalAcepted.status < 3 && <TouchableOpacity
                                style={styles.buttonNext}
                                onPress={()=>this.setState({isModalPay:true})}
                                >
                                <Text style={styles.buttonTextModal}>{"Pagar Vía Paypal"}</Text>
                            </TouchableOpacity>*/}
                            {this.state.proposalAcepted.status === 1 && <TouchableOpacity
                                style={styles.buttonNext}
                                onPress={()=>this.cancelPropousal()}
                                >
                                <Text style={styles.buttonTextModal}>{"Cancelar"}</Text>
                            </TouchableOpacity>}
                            {this.state.proposalAcepted.status === 1 && <TouchableOpacity
                                style={styles.buttonNext}
                                onPress={()=>this.setState({isModal:true})}
                                >
                                <Text style={styles.buttonTextModal}>{"Servicio Finalizado"}</Text>
                            </TouchableOpacity>}
                            {/*this.state.proposalAcepted.status < 3 && <View style={{width:'100%'}}>
                                <Text style={{fontWeight:'bold', textAlign:'center'}}>{'Si no deseas pagar con Paypal, puedes acordar el'}</Text>
                                <Text style={{fontWeight:'bold', textAlign:'center'}}>{'pago con el proveedor directamente'}</Text>
                            </View>*/}
                        </View>
                    }


              </View>
            { this.state.isAcepted && <Modal
                animationType="slide"
                transparent={true}

                visible={this.state.isModal || this.state.isModalPay}

                onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                <View style={{backgroundColor:'rgba(0,0,0,0.6)', width:'100%', height:'100%', justifyContent:'center'}}>
                    { this.state.proposalAcepted.status !== 1 ?
                        <View style={styles.modal}>
                            <TouchableOpacity
                                    style={{position:'absolute', top:5,left:5}}
                                    onPress={()=>this.setState({isModal:false, isModalPay:false})}
                                    >
                                <MaterialIcons size={30} name='clear' />
                            </TouchableOpacity>
                            <View style={{flex:0.6, alignItems:'center'}}>
                                <Text style={[styles.modalText, {marginTop:'5%'}]}>{"Pagar vía paypal"}</Text>

                                <Image
                                    style={[styles.paypalLogo,{marginTop:'7%'}]}
                                    source={require('../img/PayPal-simbolo.png')}
                                />

                                <Text style={[styles.modalText,{marginTop:'5%'}]}>{this.state.proposalAcepted.budget + " USD"}</Text>
                            </View>
                            <View style={{flex:0.1}}>
                                <TouchableOpacity
                                    style={styles.buttonModal}
                                    onPress={()=>this.openPaypalModal()}
                                    >
                                     {
    									this.state.loadingPaypal ?
    									<ActivityIndicator style={{marginTop:10}} color="#fff"/>
    									:
    									<Text style={styles.buttonTextModal}>{"Pagar"}</Text>
    								}


                                </TouchableOpacity>
                            </View>

                        </View>
                    :
                        <View style={styles.modal}>
                            <TouchableOpacity
                                    style={{position:'absolute', top:5,left:5}}
                                    onPress={()=>this.setState({isModal:false, isModalPay:false})}
                                    >
                                <MaterialIcons size={30} name='clear' />
                            </TouchableOpacity>
                            <View style={{flex:0.60,alignItems:'center'}}>
                                <Text style={[styles.modalText,{marginTop:'5%'}]}>{"Calificar a:"}</Text>
                                <Text style={[styles.modalText,{marginTop:'5%'}]}>{this.state.proposalAcepted.users.profile.name}</Text>

                                <Text style={styles.modalText}>{"Calificación"}</Text>
                                <StarRating
                                    disabled={false}
                                    maxStars={5}
                                    rating={this.state.rating}
                                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                                    fullStarColor={'#f1c40f'}
                                    starSize={20}
                                    containerStyle={{width: 150, marginTop:10}}
                                />
                            </View>
                            <View style={{flex:0.1}}>

                                <TouchableOpacity
                                    style={styles.buttonModal}
                                    onPress={()=>this.finishWork()}
                                    >

                                    <Text style={styles.buttonTextModal}>{"Finalizar"}</Text>

                                </TouchableOpacity>
                            </View>

                        </View>}
                </View>
            </Modal> }
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.paypalModal}

                onRequestClose={() => {
                    this.setState({paypalModal:false})
                }}>

                <View style={{backgroundColor:'rgba(0,0,0,0.6)', width:'100%', height:'100%'}}>

                    <WebView
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        renderLoading={()=><ActivityIndicator style={{
                            position: 'absolute',
                            left: 0,
                            right: 0,
                            top: 0,
                            bottom: 0,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }} size='large' color='white'/>}
                        startInLoadingState={true}
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            flex:1,
                        }}
                        source={{uri: this.state.urlPaypal}}/>
                </View>
            </Modal>
             <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.deleteEvent}

                onRequestClose={() => {
                    this.setState({paypalModal:false})
                }}>

                <View style={{backgroundColor:'rgba(0,0,0,0.6)', width:'100%', height:'100%',justifyContent:'center'}}>
                
                    <View style={[styles.modal, {height:150}]}>
                         <TouchableOpacity
                                style={{position:'absolute', top:5,left:5}}
                                onPress={()=>this.setState({deleteEvent:false,})}
                                >
                            <MaterialIcons size={30} name='clear' />
                        </TouchableOpacity>
                        <Text style={{textAlign:'center', marginTop:20, marginBottom:15}}>{`¿Estas seguro que quieres eliminar el evento ${this.props.navigation.state.params.event.name}?`}</Text>
                        <TouchableOpacity
                            style={styles.buttonModal}
                            onPress={this.deleteEvent}
                            >
                            {
                            this.state.loadingDelete ?
                                <ActivityIndicator style={{marginTop:10}} color="#fff"/>
                            :
                                <Text style={[styles.buttonTextModal]}>{"Eliminar"}</Text>
                            }

                        </TouchableOpacity>

                    </View>
                </View>
            </Modal>
          </ScrollView>
        );
      }
}

const styles = StyleSheet.create({
    container : {
        flex: 1,

    },

    buttonAction:{
        fontSize:23,
        color:'#ea1d75'
    },

    evento: {
        paddingRight:15,
        borderRadius: 5,
        // shadowOpacity: 0.3,
        // shadowOffset: {width:0, height:0},
        // shadowColor: 'gray',
        // shadowRadius: 10,
        marginBottom:20,
        padding:20,
        backgroundColor: "#fff",
        // borderBottomColor: "gray",
        // borderBottomWidth: 2,
    },

    eventoTitulo : {
        fontSize: 17,
        marginBottom: 5,
        color: "#DEB749"
    },

    eventoDescripcion : {
        color: "#212121"
    },

    eventoFecha : {
        marginTop: 10,
        color: "#212121"
    },

    title : {
        fontSize: 18,
        marginBottom: 15,
        textAlign:'center'
    },

    proveedor : {
        padding: 20,
        borderBottomColor: "#ecf0f1",
        borderBottomWidth: 1,
        backgroundColor: "#fff",
    },

    proveedorNombre : {
        fontSize: 17,
        marginBottom: 5,
        fontWeight: 'normal',
        color: "#DEB749"
    },

    proveedorDescripcion : {
        maxWidth:'60%'
    },

    proveedorNombrePrecio: {
        flexDirection: 'row',
        justifyContent:'space-between',

    },

    proveedorPrecio : {
        fontSize: 18,
        color: "#ea1d75",
        fontWeight: 'bold'
    },

    proveedorAction : {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    buttonNext : {
        height: 40,
        backgroundColor:"#ea1d75",
        alignSelf: 'stretch',
        width:'80%',
        marginLeft:'10%',
        marginBottom:'5%'
    },

    buttonText : {
        paddingTop: 5,
        color: "#ea1d75",
        textAlign:'center',
        fontSize: 17,
    },

    buttonTextModal : {
        color: "#fff",
        textAlign:'center',
        fontSize: 17,
        paddingVertical:10
    },
    buttonModal : {
        height: 50,
        backgroundColor:"#ea1d75",
        width:'80%',
        alignSelf: 'stretch',

        marginLeft:'10%',
        borderRadius: 3,
    },
    modal:{
        backgroundColor:'#eee',
        width:'70%',
        marginLeft:'15%',
        height:250,
        borderColor:'black',
        borderWidth:1,
        borderRadius:10,
        justifyContent:'center'

    },
    modalText:{
        color: "#000",
        textAlign:'center',
        fontSize: 17,
    },

    paypalLogo:{
        width:30,
        height:30
    }

});
