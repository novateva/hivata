import React from 'react';
import { Button, View, StyleSheet, Image, TextInput, TouchableOpacity, ScrollView, Text, ActivityIndicator } from 'react-native';
import proposal from '../services/proposal';
import ERROR from '../config/Error';
import Firebase from '../config/firebase';
import { EventRegister } from 'react-native-event-listeners';
export default class EventoDetailScreen extends React.Component {
    


    constructor(props) {
        super(props);
        this.state = {
          currencies_id:1,
          token:this.props.screenProps.user.token,
          events_id: this.props.navigation.state.params.event.id,
          status:0,
          budget:'',
          description:'',
          ProposalAcepted:{}
        };

        proposal.proposalEvent(this.props.navigation.state.params.event.id)
            .then(data=>{
                
                if(!data)return;
                
                for (var i = 0; i < data.length; i++) {
                    if (data[i].users_id === this.props.screenProps.user.user.id){
                        return this.setState({ProposalAcepted:data[i], loader:true})
                    }
                }
                this.setState({loader:true})
                
            })
        this.submitValue = this.submitValue.bind(this);
     }
    
     

    onChangeValue(name, value) {
        this.setState({[name]:value})
    }

   

    submitValue(){
        
        if(this.state.description === ""){
          ERROR("Debe introducir una descripción","Error");
          return;
        }
        if(this.state.budget === ""){
          ERROR("Debe introducir un presupuesto","Error");
          return;
        }
        
        this.setState({loading:true})
            proposal.createproposal(this.state)
              .then(data=>{
                EventRegister.emit('eventCreated','event');
                this.props.navigation.goBack();
                ERROR("Propuesta creada con éxito","Exito");
                  return;
              })
              .catch(erro=>{
                ERROR("El evento ya no existe","error")
              })
        
    }

  render() {
    
    if(!this.state.loader)
        return(
            <View style={{height:'100%', justifyContent:'center', alignItems:'center'}}>
                <ActivityIndicator size="large" color="pink"/>
            </View>
            
        );
    
    return (
        
      <ScrollView ref = {component=>{this._scrollView=component;}}>
          <View style={styles.container}>
            <View style={styles.evento}>
                <Text style={styles.eventoTitulo}>{this.props.navigation.state.params.event.name}</Text>
                <Text style={styles.eventoDescripcion}>{this.props.navigation.state.params.event.description}</Text>
                <Text style={styles.eventoFecha}>{`${this.props.navigation.state.params.event.date.split(' ')[0]} ${this.props.navigation.state.params.event.city}`}</Text>
                <Text style={styles.eventoDescripcion}>{this.props.navigation.state.params.event.location}</Text>
            </View>

            <Text style={styles.title}>Propuesta</Text>

          </View>
          
          
            {
                this.state.ProposalAcepted.status !== undefined
                ? 
                <View style={styles.evento}>
                    <Text style={styles.label}>{"Propuesta Enviada"}</Text>
                    <Text style={styles.label}>{"Estatus: " + (this.state.ProposalAcepted.status === 0 ? 'Enviada' : (this.state.ProposalAcepted.status === 1 ? 'Aceptada' : (this.state.ProposalAcepted.status !== 2 ? 'Pagada' : 'Finalizada')))}</Text>
                </View>
                :
                <View style={styles.propuesta}>
                    <Text style={styles.label}>{"Descripción Propuesta"}</Text>
                    <TextInput 
                    style={styles.textarea}
                    placeholder=""
                    placeholderTextColor= '#ccc'
                    multiline={true}
                    numberOfLines={10}
                     onChangeText={(text)=>this.onChangeValue('description', text)}
                    underlineColorAndroid={'rgba(0,0,0,0)'}>
                   
                    </TextInput>

                    <Text style={styles.label}>{'Presupuesto'}</Text>
                    
                    <TextInput 
                    style={styles.input}
                    placeholder=""
                    onFocus={()=>setTimeout(() => {this._scrollView.scrollToEnd({animated: true})}, 500) }
                    placeholderTextColor= '#ccc'
                    keyboardType='numeric'
                    onChangeText={(text)=>this.onChangeValue('budget', text)}
                    underlineColorAndroid={'rgba(0,0,0,0)'}>
                    </TextInput>

                   
                    <TouchableOpacity 
                        style={styles.button}
                        onPress={this.submitValue}
                        >
                        {
                          this.state.loading ?
                          <ActivityIndicator color="#fff"/>
                          :
                          <Text style={styles.buttonText}>Publicar Propuesta</Text>  
                        }
                    </TouchableOpacity>
                </View>
            }
                
                
          

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
    container : {
        flex: 1,
        
    },
    evento: {
    backgroundColor: "#fff",
    padding: 10,
    paddingRight:15,
    borderRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: {width:0, height:0},
    shadowColor: 'gray',
    shadowRadius: 10,
    marginBottom:20,
    // borderBottomColor: "gray",
    // borderBottomWidth: 2,
    },
    evento: {
        paddingRight:15,
        borderRadius: 5,
        // shadowOpacity: 0.3,
        // shadowOffset: {width:0, height:0},
        // shadowColor: 'gray',
        // shadowRadius: 10,
        marginBottom:20,
        padding:20,
        backgroundColor: "#fff",
        // borderBottomColor: "gray",
        // borderBottomWidth: 2,
    },

    eventoTitulo : {
        fontSize: 17,
        marginBottom: 5,
        color: "#DEB749"
    },

    eventoDescripcion : {
        color: "#212121"
    },

    eventoFecha : {
        marginTop: 10,
        color: "#212121"
    },

    title : {
        fontSize: 18,
        marginBottom: 15,
        textAlign:'center'
    },

    propuesta: {
        padding:20,
    },

    label : {
        fontSize: 17,
        color: "#666",
        marginBottom: 3
    },

    input: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        borderRadius: 3,
    },

    textarea: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        height: 100,
        textAlignVertical: "top",
        borderRadius: 3,
    },

    button : {
        height: 50,
        backgroundColor:"#ea1d75",
        // backgroundColor:"#feda73",
        alignSelf: 'stretch',
        paddingVertical: 15,
        borderRadius: 3,
    },

    buttonText : {
        color: "#fff",
        textAlign:'center',
        fontSize: 17,
    },

});


