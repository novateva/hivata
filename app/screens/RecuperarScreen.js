import React from 'react';
import { ActivityIndicator, Text, View, StyleSheet, Image, TextInput, ScrollView, TouchableOpacity, StatusBar } from 'react-native';
import user from '../services/user';
import ERROR from '../config/Error';
export default class RecuperarScreen extends React.Component {
  static navigationOptions = {
    header:null
  }
  constructor(props){
    super(props);
    this.state ={
      email:''
    }
  }
  submit(){
    if(this.state.email === ''){
      ERROR('Díganos su correo','Error');
      return;
    }
    this.setState({loading:true})
    user.exitsUser({email:this.state.email}).then(result=>{
      console.log('resukt',result)
       if (result.status === 'success'){
         ERROR("Contraseña enviada a su correo",'Éxito');
              this.props.navigation.goBack();
         user.recovery({email:this.state.email})
          .then((data)=>{console.log('data',data)}).catch(error=>{
            

          })
         
        } else {
          ERROR("Correo no registrado",'Error');
        }
        this.setState({loading:false})
    })
    
  }
  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle = "light-content" backgroundColor="#000" />
        <ScrollView ref = {component=>{this._scrollView=component;}} contentContainerstyle={[styles.container, {width:'100%', backgroundColor:'white'}]}>
          <View style={{justifyContent:'center', alignItems:'center'}}>	
						<Image
							style={styles.logo}
							source={require('../img/logo.png')}
						/> 
					</View>
					 
          
          <TextInput 
            style={styles.input}
            placeholder="Correo Electrónico"
            placeholderTextColor='rgba(255,255,255, 0.7)'
            underlineColorAndroid={'rgba(0,0,0,0)'}
            onChangeText={(email)=>this.setState({email})}
            onFocus={()=>setTimeout(() => {this._scrollView.scrollToEnd({animated: true})}, 500) }

            >
          </TextInput>

        
          <TouchableOpacity 
            style={styles.button}
            onPress={() => this.submit()}
            >
            {
              this.state.loading ?
              <ActivityIndicator color="#fff"/>
              :
              <Text style={styles.buttonText}>{'Recuperar'}</Text>  
            }
          </TouchableOpacity>

          <TouchableOpacity 
            style={styles.link}
            onPress={() => this.props.navigation.navigate('Login')}
            >
            <Text style={styles.buttonText}>Iniciar Sesión</Text>  
          </TouchableOpacity>  
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#000",
    padding: 20,
  },

  input: {
    padding: 10,
    backgroundColor:'rgba(255, 255, 255, 0.3)',
    height: 50,
    alignSelf: 'stretch',
    color:"#fff",
    fontSize: 17,
    marginBottom: 30,
    borderRadius: 3,
    width:'100%'
  },


  logo : {
    width: 200,
    resizeMode: 'contain'
  },

  button : {
    height: 50,
    backgroundColor:"#ea1d75",
    // backgroundColor:"#feda73",
    alignSelf: 'stretch',
    paddingVertical: 15,
    borderRadius: 3,
  },

  buttonText : {
    color: "#fff",
    textAlign:'center',
    fontSize: 17,
  },

  link : {
    marginTop: 30,
  }




});


