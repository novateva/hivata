import React from 'react';
import { Text, View, StyleSheet, ScrollView, TextInput, TouchableOpacity } from 'react-native';
import Ionicons from "react-native-vector-icons/Ionicons";
import Firebase from '../config/firebase';
import Proposal from '../services/proposal';
import Moment from 'moment';

export default class ProveedorScreen extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      starCount: 3.5,
      chat:[],
      userId: this.props.screenProps.user.user.id,
      text:''
    };
    
    this.insertMesaje = this.insertMesaje.bind(this);
    
  }
  componentDidMount() {

      this.ref = Firebase.database().ref(`chat/${this.state.userId}/${this.props.navigation.state.params.idEvent}`)
        .on('value', (dataSnapshot) => { 
            if (!dataSnapshot.val()) {
                this.props.navigation.goBack();
                return;
            }
            const data = dataSnapshot.val();
            
            this.setState({
                chat: data.message ? data.message : [], 
                idParnet: this.state.userId === data.eventOwnerId ? data.proposalOnwerId :data.eventOwnerId 
            })
            setTimeout(()=> {
              this._scrollView.scrollToEnd({animated: true})  
            }, 10);
            

            
        });
  }
  componentWillUnmount() {
    Firebase.database().ref(`chat/${this.state.userId}/${this.props.navigation.state.params.idEvent}`).off('value', this.ref)
  }  

  insertMesaje(){
    if (this.state.text === '') {
        return;
    }
    
    Firebase.database().ref(`chat/${this.state.userId}/${this.props.navigation.state.params.idEvent}/message`).push({
        text:this.state.text,
        owner:this.state.userId,
        date:Moment().format("H:mm:ss A")
    });
    Firebase.database().ref(`chat/${this.state.idParnet}/${this.props.navigation.state.params.idEvent}/message`).push({
        text:this.state.text,
        owner:this.state.userId,
        date:Moment().format("H:mm:ss A")
    })
    Proposal.sendNotifications({events_id:this.props.navigation.state.params.idEvent, iduser:this.state.idParnet, title:'Mensaje nuevo', body:this.state.text, token:this.props.screenProps.user.token})
        .then(data=>{console.log(data)})
    this.state.text = '';


  }

  render() {
    
    return (
      
        <View style={styles.container}>
            <ScrollView ref = {component=>{this._scrollView=component;}}>
                {   
                    Object.keys(this.state.chat).map(keyChat=>{
                        if(this.state.userId === this.state.chat[keyChat].owner){
                            return (
                                <View style={styles.containerMensajes}>
                                    <View style={styles.mensajeRight}>
                                        <Text style={{padding:5}}>{this.state.chat[keyChat].text}</Text>
                                        <Text style={styles.timeMsj}>{this.state.chat[keyChat].date}</Text>
                                    </View>
                                </View>
                            )
                        } else {
                            return(
                                <View style={styles.containerMensajes}>
                                    <View style={styles.mensajeLeft}>
                                        <Text style={{padding:5}} >{this.state.chat[keyChat].text}</Text>
                                        <Text style={styles.timeMsj}>{this.state.chat[keyChat].date}</Text>
                                    </View>
                                </View>
                            );
                            
                        }
                    })
                }
            </ScrollView>
            


            <View style={styles.containerInputMensaje}>
                <TextInput 
                    style={styles.input}
                    placeholder="Contraseña"
                    onFocus={()=>setTimeout(() => {this._scrollView.scrollToEnd({animated: true})}, 500) }
                    onSubmitEditing={()=>this.insertMesaje()}
                    value={this.state.text}
                    placeholderTextColor= 'rgba(255,255,255, 0.7)'
                    onChangeText={(text)=>this.setState({text})}
                    underlineColorAndroid={'rgba(0,0,0,0)'}>
                </TextInput>
                <TouchableOpacity onPress={this.insertMesaje}>
                    <Ionicons name='md-send' size={35} color="#ea1d75" style={styles} />
                </TouchableOpacity>
            </View>
        </View>
      
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        padding: 10,
        
    },

    containerMensajes : {
        
    },

    mensajeRight : {
        paddingVertical: 10,
        backgroundColor: '#ccc',
        marginBottom: 10,
        marginLeft: 60,
        padding:10,
        borderRadius: 3,
    },

    mensajeLeft : {
        paddingVertical: 10,
        backgroundColor: '#fec53d',
        marginBottom: 10,
        marginRight: 60,
        padding:10,
        borderRadius: 3,
    },

    containerInputMensaje: {
        flexDirection: 'row',
        marginTop: 'auto',
        justifyContent:'space-between',
        alignItems: 'center',
        
    },

    input: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        borderRadius: 3,
        flex:1,
        marginRight:10
    },

    timeMsj : {
        position:'absolute',
        fontSize:9,
        bottom:5,
        right:5

    },

});


