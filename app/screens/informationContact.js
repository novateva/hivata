import React from 'react';
import { Button, View, StyleSheet, Image, TextInput, TouchableOpacity, ScrollView, Text, ActivityIndicator } from 'react-native';
import Firebase from '../config/firebase';
import ERROR from '../config/Error';

import { EventRegister } from 'react-native-event-listeners';

export default class EventosCreateScreen extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      token:this.props.screenProps.user.token,
      currencies_id:1,
      latitude:3.23,
      longitude:3.23,
      asunto:'',
      descriction:'',
      dia:'',
      mes:'',
      anio:'',
      date:'',
      location:'',

    };

    console.log(this.props.navigation.state.params)
    this.submitValue = this.submitValue.bind(this)
  }

  onChangeValue(name, value) {
    this.setState({[name]:value})
  }

  submitValue(){

    Firebase.database().ref(`chat/${this.props.screenProps.user.user.id}/${'direct'+this.props.screenProps.user.user.id + this.props.navigation.state.params.user.id}`).set({proposal:{users:{profile:this.props.navigation.state.params.profile,...this.props.navigation.state.params.user}}, event:{name:'Contacto Directo', id:null} ,eventOwnerId:this.props.screenProps.user.user.id, proposalOnwerId: this.props.navigation.state.params.user.id, message:[]});
    Firebase.database().ref(`chat/${this.props.navigation.state.params.user.id}/${'direct'+this.props.screenProps.user.user.id + this.props.navigation.state.params.user.id}`).set({proposal:{users:{profile:this.props.screenProps.user.profile,...this.props.screenProps.user.user}}, event:{name:'Contacto Directo', id:null} ,eventOwnerId:this.props.screenProps.user.user.id, proposalOnwerId: this.props.navigation.state.params.user.id, message:[]});
    ERROR("Chat creado con éxito", 'Éxito');
    this.props.navigation.navigate('DetailMensaje', {idEvent:'direct'+this.props.screenProps.user.user.id + this.props.navigation.state.params.user.id});
  }

  render() {

    return (

      <ScrollView style={styles.container} >
        <Text style={[styles.label,{textAlign:'center',fontWeight:'bold', fontSize:19}]}>{`Datos de contacto`}</Text>
        <Text style={[styles.label,{fontWeight:'bold', fontSize:19}]}>{`Persona de contacto: ${(this.props.navigation.state.params.profile.contactName && this.props.navigation.state.params.profile.contactName !== 'null' )   ? this.props.navigation.state.params.profile.contactName : 'No proporcionado'}`}</Text>
        <Text style={[styles.label,{fontWeight:'bold', fontSize:19}]}>{`Email: ${(this.props.navigation.state.params.profile.contactEmail && this.props.navigation.state.params.profile.contactEmail !=='null' )? this.props.navigation.state.params.profile.contactEmail : 'No proporcionado'}`}</Text>
        <Text style={[styles.label,{fontWeight:'bold', fontSize:19}]}>{`Telefono: ${(this.props.navigation.state.params.profile.phone && this.props.navigation.state.params.profile.phone !== 'null' )? this.props.navigation.state.params.profile.phone : 'No proporcionado'}`}</Text>
        <Text style={[styles.label,{fontWeight:'bold', fontSize:19}]}>{`Celular: ${(this.props.navigation.state.params.profile.cellPhone && this.props.navigation.state.params.profile.cellPhone !== 'null') ? this.props.navigation.state.params.profile.cellPhone : 'No proporcionado'}`}</Text>

        <TouchableOpacity
            style={styles.button}
            onPress={this.submitValue}
            >
            {
            this.state.loading ?
            <ActivityIndicator color="#fff"/>
            :
            <Text style={styles.buttonText}>Enviar Mensaje</Text>
          }
          </TouchableOpacity>

        <View style={{backgroundColor:'transparent', height:50}} />


      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
    container : {
        padding:20,
        paddingBottom: 40,
    },

    label : {
      fontSize: 17,
      color: "#666",
      marginBottom: 3
    },
    input: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        borderRadius: 3,
    },

    picker: {
      backgroundColor:'rgba(255, 255, 255, 1)',
      height: 50,
      width:'100%',
      color:"#212121",
      alignSelf: 'stretch',
      padding:10
    },

    textarea: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        height: 100,
        textAlignVertical: "top",
        borderRadius: 3,
    },

    button : {
        height: 50,
        backgroundColor:"#ea1d75",
        // backgroundColor:"#feda73",
        alignSelf: 'stretch',
        paddingVertical: 15,
        borderRadius: 3,
        marginTop:'3%'
    },

    buttonText : {
        color: "#fff",
        textAlign:'center',
        fontSize: 17,
    },
});
