import React from 'react';
import { Button, View, StyleSheet, TouchableHighlight, TextInput, TouchableOpacity, ScrollView, Text, ActivityIndicator, Image } from 'react-native';
import User from '../services/user';
import Ionicons from "react-native-vector-icons/Ionicons";
import ERROR from '../config/Error';
import Imagen from '../services/imagenSlider'
import { EventRegister } from 'react-native-event-listeners';
import expo from 'expo';

export default class EventosCreateScreen extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      token:this.props.screenProps.user.token,
      user:this.props.screenProps.user.user,
      currencies_id:1,
      latitude:3.23,
      longitude:3.23,
      asunto:'',
      descriction:'',
      dia:'',
      mes:'',
      anio:'',
      date:'',
      location:'',
      imagenUser : this.props.screenProps.user.user.roles_id === 1 ? require('../img/proveedor.png') : require('../img/cliente.png')

    };


    this.submitValue = this.submitValue.bind(this);
    this.addImage = this.addImage.bind(this);
  }

  onChangeValue(name, value) {
    this.setState({[name]:value})
  }

  submitValue(){


    if(this.state.asunto === ""){
      ERROR("Debe introducir un asunto","Error");
      return;
    }
    if(this.state.descripcion === ""){
      ERROR("Debe introducir una descripción","Error");
      return;
    }
    this.setState({loading:true})
    User.SendPost({name:this.props.screenProps.user.profile.name, email:this.props.screenProps.user.user.email, token:this.props.screenProps.user.token, asunto:this.state.asunto, descripcion:this.state.descripcion})
        .then(data=>{
            this.setState({asunto:'', descripcion:'',loading:false})
          ERROR("Mensaje enviando con éxito","Éxito");

        })
        .catch((err)=>{
          console.log(err)
          this.setState({loading:false})
        })
  }

  addImage() {

      expo.ImagePicker.launchImageLibraryAsync({
          mediaTypes:'Images',
          allowsEditing:true,
          width: 400,
          aspect:[1,1],
          quality:0.5,
          height: 100,
          cropping: true
      })
      .then(result => {
          if(!result.cancelled){
              this.setState({loadingAddImage:true})
              Imagen.UploadPerfil({ uri: result.uri, type: `${result.type}/${result.uri.substr(result.uri.lastIndexOf('.')+1)}`, name:('imagenPerfil' + String( this.props.screenProps.user.user.id)) }, this.props.screenProps.user.user.id )
                  .then(data=>{

                    this.setState({user:data.user, loadingAddImage:false});
                    this.props.screenProps.user.user= data.user;
                    ERROR("Imagen actualizada con éxito","Éxito");

                  })
                  .catch(erro=>{console.log(erro)});
          }
      })

  }

  render() {
    return (

      <ScrollView ref = {component=>{this._scrollView=component;}} style={styles.container} ref = {component=>{this._scrollView=component;}}>
        <View style={{position:'relative', width:'100%', height:150, alignItems:'center'}}>
            <View style={{width:150, height:150, position:'relative'}}>
                <Image
                  style={styles.imagen}
                  source={this.state.user.imagenPath === ''? this.state.imagenUser :
                   {uri:'http://198.211.114.51/api/retrieve-image/'+this.state.user.imagenPath}}
                />
                <TouchableOpacity
                    style={styles.buttonPlusSlider}
                    onPress={this.addImage}>
                    {

                        this.state.loadingAddImage ?
                        <ActivityIndicator color="#fff"/>
                        :
                        <Ionicons style={styles.buttonText} name='md-create'/>


                    }
                </TouchableOpacity>
            </View>
        </View>

        <Text style={[styles.label,{textAlign:'center', fontWeight:'bold', fontSize:19, marginTop:'2%'}]}>{this.props.screenProps.user.profile.name}</Text>
        <Text style={[styles.label,{textAlign:'center', fontWeight:'bold', fontSize:19}]}>{this.props.screenProps.user.user.email}</Text>
        <TouchableOpacity
            style={styles.button}
            onPress={()=>EventRegister.emit('logOut')}
            >
                <Text style={styles.buttonText}>{'Cerrar sesión'}</Text>
        </TouchableOpacity>
        <View style={{width:'100%', height:2, backgroundColor:'#ea1d75', marginTop:'5%',marginBottom:'5%'}}></View>
        <Text style={[styles.label, {textAlign:'center'}]}>{'Contáctanos'}</Text>

        <Text style={styles.label}>{'Asunto'}</Text>
        <TextInput
          style={styles.input}
          placeholder={'Asunto'}
          value={this.state.asunto}
          onSubmitEditing={()=>this._location.focus()}
          placeholderTextColor='#ccc'
          onChangeText={(text)=>this.onChangeValue('asunto', text)}
          underlineColorAndroid={'rgba(0,0,0,0)'}
          >
        </TextInput>

        <Text style={styles.label}>{'Descripción'}</Text>
        <TextInput
            onTouchStart={()=>{setTimeout(() => {this._scrollView.scrollToEnd({animated: true})}, 500); }}
            style={styles.textarea}
            value={this.state.descripcion}
            placeholder="Descripción"
            ref={component => this._location = component}
            placeholderTextColor= '#ccc'
            multiline={true}
            numberOfLines={10}
            onChangeText={(text)=>this.onChangeValue('descripcion', text)}
            underlineColorAndroid={'rgba(0,0,0,0)'}>

        </TextInput>

        <TouchableOpacity
            style={styles.button}
            onPress={this.submitValue}
            >
            {
            this.state.loading ?
            <ActivityIndicator color="#fff"/>
            :
            <Text style={styles.buttonText}>Enviar</Text>
          }
          </TouchableOpacity>

        <View style={{backgroundColor:'transparent', height:50}} />


      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
    container : {
        padding:20,
        paddingBottom: 40,
    },

    label : {
      fontSize: 17,
      color: "#666",
      marginBottom: 3
    },
    input: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        borderRadius: 3,
    },

    picker: {
      backgroundColor:'rgba(255, 255, 255, 1)',
      height: 50,
      width:'100%',
      color:"#212121",
      alignSelf: 'stretch',
      padding:10
    },

    textarea: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        height: 100,
        textAlignVertical: "top",
        borderRadius: 3,
    },

    button : {
        height: 50,
        backgroundColor:"#ea1d75",
        // backgroundColor:"#feda73",
        alignSelf: 'stretch',
        paddingVertical: 15,
        borderRadius: 3,
        marginTop:'3%'
    },

    buttonText : {
        color: "#fff",
        textAlign:'center',
        fontSize: 17,
    },
    imagen :{
        width:150,
        height:150,
        position:'relative',
        borderRadius:250,
        borderColor:'gray',
        borderWidth:2
    },
    buttonPlusSlider : {
        height: 30,
        backgroundColor:"#ea1d75",
        // backgroundColor:"#feda73",
        alignSelf: 'stretch',
        paddingVertical: 5,
        width:30,
        borderRadius:250,
        position:'absolute',
        bottom: 10,
        right: 10
    },

});
