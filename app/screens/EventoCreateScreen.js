import React from 'react';
import { Button, View, StyleSheet, Image, TextInput, TouchableOpacity, ScrollView, Text, ActivityIndicator } from 'react-native';
import Event from '../services/events';
import ERROR from '../config/Error';
import Moment from 'moment';
import Select from 'react-native-picker-select';
import { EventRegister } from 'react-native-event-listeners';

export default class EventosCreateScreen extends React.Component {

  constructor(props) {
    super(props);
  
    this.state = {
      token:this.props.screenProps.user.token,
      currencies_id:1,
      latitude:3.23,
      longitude:3.23,
      name:'',
      description:'',
      dia:'',
      mes:'',
      anio:'',
      date:'',
      location:'',
      city:''
     
    };
    this.items = [
			"Aguascalientes",
 			"Baja California",
      "Baja California Sur",
      "Campeche",
      "Chiapas",
      "Chihuahua",
      "Ciudad de México",
      "Coahuila de Zaragoza",
      "Colima",
      "Durango",
      "Estado de México",
      "Guanajuato",
      "Guerrero",
      "Hidalgo",
      "Jalisco",
      "Michoacán de Ocampo",
      "Morelos",
      "Nayarit",
      "Nuevo León",
      "Oaxaca",
      "Puebla",
      "Querétaro",
      "Quintana Roo",
      "San Luis Potosí",
      "Sinaloa",
      "Sonora",
      "Tabasco",
      "Tamaulipas",
      "Tlaxcala",
      "Veracruz de Ignacio de la Llave",
      "Yucatán",
      "Zacatecas",
    ]
    
    this.submitValue = this.submitValue.bind(this);

  }
  
  componentWillMount() {
    if (this.props.navigation.state.params) {
      const date = this.props.navigation.state.params.event.date.substr(0,10).split('-');
      const anio = date[0], 
            mes = date[1],
            dia = date [2];
      this.setState({...this.props.navigation.state.params.event, dia, mes, anio })
    }
  }
  onChangeValue(name, value) {
    this.setState({[name]:value})
  }

  submitValue(){
    
   
    if(this.state.name === ""){
      ERROR("Debe introducir un nombre","Error");
      return;
    }
    if(this.state.description === ""){
      ERROR("Debe introducir una descripción","Error");
      return;
    }
    const date = Moment(this.state.anio + '-' + this.state.mes + '-' + this.state.dia, '')
    
    if(!date.isValid()){
      ERROR("Debe introducir un fecha de evento valida","Error");
      return;
    } else if (new Date().getTime() > new Date(this.state.anio + '/' + this.state.mes + '/' + this.state.dia).getTime()) {
      ERROR("Debe introducir un fecha posterior a la de hoy","Error");
      return;
    }

    if( this.state.city === ""){
      ERROR("Debe introducir una estado","Error");
      return;
    } 

    if( this.state.location === ""){
      ERROR("Debe introducir una localización","Error");
      return;
    } 
    

      this.state.date = this.state.anio + '/' + this.state.mes + '/' + this.state.dia
      this.setState({loading:true})
      if (!this.props.navigation.state.params) {
        Event.createEvent(this.state)
        .then(data=>{
          EventRegister.emit('eventCreated','event');
          ERROR("Evento creada con éxito","éxito");
          this.props.navigation.goBack();
        })
        .catch((err)=>{
          console.log(err)
          this.setState({loading:false})
        })
      } else {
        Event.Update(this.state, this.state.id)
        .then(data=>{
          console.log(data)
          EventRegister.emit('eventCreated','event');
          ERROR("Evento actualizado con éxito","éxito");
          this.props.navigation.navigate('Eventos');
        })
        .catch((err)=>{
          console.log(err)
          this.setState({loading:false})
        })
      }
      
  }

  render() {
    
    return (
        
      <ScrollView ref = {component=>{this._scrollView=component;}} style={styles.container}>
        
        <Text style={styles.label}>{'Nombre'}</Text>
        <TextInput 
          style={styles.input}
          placeholder=""
          ref={component => this._name = component}
          placeholderTextColor='#ccc'
          value={this.state.name}
          onSubmitEditing={()=>this._description.focus()}
          onChangeText={(text)=>this.onChangeValue('name', text)}
          underlineColorAndroid={'rgba(0,0,0,0)'}
          >
        </TextInput>


        <Text style={styles.label}>{'Descripción'}</Text>
        <TextInput 
          style={styles.textarea}
          placeholder=""
          ref={component => this._description = component}
          onSubmitEditing={()=>this._date.focus()}
          placeholderTextColor= '#ccc'
          multiline={true}
          value={this.state.description}
          numberOfLines={10}
          onChangeText={(text)=>this.onChangeValue('description', text)}
          underlineColorAndroid={'rgba(0,0,0,0)'}>
          
          
        </TextInput>

        <Text style={styles.label}>{'Fecha del evento'}</Text>
          
        <View style={{'flexDirection':'row'}}>  
          <TextInput 
            style={[styles.input, {'width':'31%'}]}
            placeholder="DD"
            keyboardType='numeric'
            ref={component => this._date = component}
            onSubmitEditing={()=>this._month.focus()}
            maxLength={2}
            value={this.state.dia}
            onChangeText={(text)=>this.onChangeValue('dia', text)}
            placeholderTextColor= '#ccc'
            underlineColorAndroid={'rgba(0,0,0,0)'}>
          </TextInput>
          <TextInput 
            style={[styles.input, {'marginLeft':'2%', 'width':'31%'}]}
            placeholder="MM"
            keyboardType='numeric'
            maxLength={2}
            ref={component => this._month = component}
            onSubmitEditing={()=>this._year.focus()}
            onChangeText={(text)=>this.onChangeValue('mes', text)}
            value={this.state.mes}
            placeholderTextColor= '#ccc'
            underlineColorAndroid={'rgba(0,0,0,0)'}>
          </TextInput>
          <TextInput 
            style={[styles.input, {'marginLeft':'2%', 'width':'31%'}]}
            placeholder="AAAA"
            keyboardType='numeric'
            maxLength={4}
            ref={component => this._year = component}
            
            onChangeText={(text)=>this.onChangeValue('anio', text)}
            value={this.state.anio}
            placeholderTextColor= '#ccc'
            underlineColorAndroid={'rgba(0,0,0,0)'}>
          </TextInput>
        </View>
        <Text style={styles.label}>{'Estado'}</Text>
        <View style={{backgroundColor:'white', marginBottom:20, borderRadius:3}}>
          <Select
              placeholder={{
                  label: 'Selecciona estado...',
                  value: '',
              }}
              items={this.items.map(data=>({label:data, value:data}))}
              onValueChange={(value) => {this.onChangeValue('city', value);this._location.focus()}}
              value={this.state.city}
              style={{
                padding: 10,
                backgroundColor:'rgba(255, 255, 255, 1)',
                height: 50,
                alignSelf: 'stretch',
                color:"#212121",
                fontSize: 17,
                marginBottom: 20,
                borderRadius: 3,
                underline:{
									borderTopColor:'transparent'
								}
            }}
          />
        </View>
        <Text style={styles.label}>{'Municipio y Código Postal'}</Text>
        <TextInput 
          style={styles.textarea}
          placeholder=""
          ref={component => this._location = component}
          placeholderTextColor= '#ccc'
          multiline={true}
          onFocus={()=>this._scrollView.scrollToEnd({animated: true})}
          numberOfLines={10}
          onChangeText={(text)=>this.onChangeValue('location', text)}
          value={this.state.location}
          underlineColorAndroid={'rgba(0,0,0,0)'}>
          
          
        </TextInput>
        
        
          <TouchableOpacity 
            style={styles.button}
            onPress={this.submitValue}
            >
            {
            this.state.loading ?
            <ActivityIndicator color="#fff"/>
            :
            <Text style={styles.buttonText}>{ this.props.navigation.state.params ? 'Actualizar Evento':'Publicar Evento'}</Text>  
          }
          </TouchableOpacity>
        <View style={{backgroundColor:'transparent', height:50}} />
         

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
    container : {
        padding:20,
        paddingBottom: 40,
    },

    label : {
      fontSize: 17,
      color: "#666",
      marginBottom: 3
    },
    input: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        borderRadius: 3,
    },

    picker: {
      backgroundColor:'rgba(255, 255, 255, 1)',
      height: 50,
      width:'100%',
      color:"#212121",
      alignSelf: 'stretch',
      padding:10
    },

    textarea: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        height: 100,
        textAlignVertical: "top",
        borderRadius: 3,
    },

    button : {
        height: 50,
        backgroundColor:"#ea1d75",
        // backgroundColor:"#feda73",
        alignSelf: 'stretch',
        paddingVertical: 15,
        borderRadius: 3,
        marginTop:'3%'
    },

    buttonText : {
        color: "#fff",
        textAlign:'center',
        fontSize: 17,
    },
});


/*
<Select
          value={this.state.category}
          style={styles.picker}
          onValueChange={(itemValue, itemIndex) => this.setState({category:itemValue, categories_id: this.state.categories[itemIndex].id})}
          items={this.state.categories.map(data=>({label: data.name ,value: data.name ,key: data.id , color: '#ea1d75'}))}
        />
        
 */