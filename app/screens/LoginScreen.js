import React from 'react';
import { ScrollView, Text, View, StyleSheet, Image, TextInput, TouchableOpacity, StatusBar, ActivityIndicator } from 'react-native';
import User from '../services/user';
import ERROR from '../config/Error';


export default class LoginScreen extends React.Component {
  static navigationOptions = {
    header:null
  }

  constructor(props) {
    super(props);
  
    this.state = {
      'Email':'',
      'Password':'',
      'activity':false
    };
  }

  onChangeValue(name, value){
    this.setState({[name]:value})
  }

  _validLogin(){
    let bandera = false;

    if (this.state.Email === "") {
      bandera = true;
      ERROR("Debe introducir un correo","Error");
      return;
      
    }

    if (this.state.Password === "") {
      bandera = true;
      ERROR("Debe introducir una contraseña", "Error");
      return;
    }

    if (!bandera) {
      this.setState({activity:true})
      User.login(this.state.Email, this.state.Password)
      .then(
        (dataToken) => {
          console.log(dataToken)
          if(!dataToken.error){
            User.me(dataToken.access_token)
              .then(dataUser=>{
                if(dataUser.user.validation){
                  dataUser.user.password = this.state.Password;
                  this.props.screenProps.logIn({token:dataToken.access_token, user:dataUser.user, profile:dataUser.profile});
                  
                } else {
                  ERROR("Valida tu correo electrónico!!", "Error");
                }
                this.setState({activity:false})
              })
          }else {
            ERROR("Credenciales inválidas", "Error");
            this.setState({activity:false})
            return;
          }
        }
      )
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle = "light-content" backgroundColor="#000" />
        <Image
          style={styles.logo}
          source={require('../img/logo.png')}
        /> 
        
        <TextInput 
          style={styles.input}
          placeholder="Correo electrónico"
          ref={component => this._login = component}
          onSubmitEditing={()=>this._password.focus()}
          placeholderTextColor='rgba(255,255,255, 0.7)'
          underlineColorAndroid={'rgba(0,0,0,0)'}
          onChangeText={(text)=>this.onChangeValue('Email', text)}
          >
        </TextInput>

        <TextInput 
          style={styles.input}
          placeholder="Contraseña"
          secureTextEntry
          ref={component => this._password = component}
          onSubmitEditing={()=>{}}
          placeholderTextColor= 'rgba(255,255,255, 0.7)'
          underlineColorAndroid={'rgba(0,0,0,0)'}
          onChangeText={(text)=>this.onChangeValue('Password', text)}
          >
          
        </TextInput>

        
        <TouchableOpacity 
          style={styles.button}
          onPress={this._validLogin.bind(this)}
          >
          {!this.state.activity ?
          <Text style={styles.buttonText}>Ingresar</Text>  
          :
          <ActivityIndicator size="small" color="#fff" />
          }
        </TouchableOpacity>
        

        <TouchableOpacity 
          style={styles.buttonSecondary}
          onPress={() => this.props.navigation.navigate('Registro')}
          >
          <Text style={styles.buttonText}>Registrarse</Text>  
        </TouchableOpacity>  


        <TouchableOpacity
          style={styles.link}
          onPress={() => this.props.navigation.navigate('Recuperar')}
          >
          <Text style={styles.buttonText}>¿ Olvidaste tu contraseña ?</Text>  
        </TouchableOpacity>  

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#000",
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
  },

  input: {
    padding: 10,
    backgroundColor:'rgba(255, 255, 255, 0.3)',
    height: 50,
    alignSelf: 'stretch',
    color:"#fff",
    fontSize: 17,
    marginBottom: 30,
    borderRadius: 3,
  },


  logo : {
    width: 200,
    height: 150,
    marginBottom: 50,
    resizeMode: 'contain'
  },

  button : {
    height: 50,
    backgroundColor:"#ea1d75",
    // backgroundColor:"#feda73",
    alignSelf: 'stretch',
    paddingVertical: 15,
    borderRadius: 3,
  },

  buttonSecondary : {
    height: 50,
    //backgroundColor:"#ea1d75",
    //backgroundColor:"#feda73",
    backgroundColor:"#fec53d",
    alignSelf: 'stretch',
    paddingVertical: 15,
    borderRadius: 3,
    marginTop: 30
  },


  buttonText : {
    color: "#fff",
    textAlign:'center',
    fontSize: 17,
  },

  link : {
    marginTop: 30,
  }
});


