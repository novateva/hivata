import React from "react";
import { Platform, View, TouchableOpacity } from "react-native";

import Ionicons from "react-native-vector-icons/Ionicons";
import LocalStorage from './LocalStorage';
import {
  createStackNavigator,
  createBottomTabNavigator,
  SafeAreaView
} from "react-navigation";
import { EventRegister } from 'react-native-event-listeners';
import LoginScreen from "../screens/LoginScreen";
import RegistroScreen from "../screens/RegistroScreen";
import RecuperarScreen from "../screens/RecuperarScreen";

import PerfilScreen from "../tabs/PerfilScreen";
import contactInformationScreen from "../screens/informationContact";
import ContactPerfilScreen from "../screens/PerfilContact";
import Menu, { MenuItem } from '../components/menu';
import EventosScreen from "../tabs/EventosScreen";
import MensajesScreen from "../tabs/MensajesScreen";
import HistorialScreen from "../tabs/HistorialScreen";
import ProveedoresScreen from "../tabs/ProveedoresScreen";

import EventoCreateScreen from "../screens/EventoCreateScreen";
import DetailProveedoresScreen from "../screens/DetailProveedoresScreen";
import EventoDetailScreen from "../screens/EventoDetailScreen";
import DetailMensajeScreen from "../screens/DetailMensajeScreen";

// Proveedor
import PropuestaScreen from "../screens/PropuestaScreen";





if (Platform.OS === 'android') {
  SafeAreaView.setStatusBarHeight(0);
}



export const AuthStack = createStackNavigator(
  {
    Login: {
      screen: LoginScreen
    },
    Registro: {
      screen: RegistroScreen
    },
    Recuperar :{
      screen:RecuperarScreen
    },
  },
 { headerMode: 'screen' }
);

const EventosStackProveedor = createStackNavigator({
  Eventos: {
    screen: EventosScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Eventos',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
      headerRight: <TouchableOpacity onPress={() =>showMenu()}>
                    <Menu
                      ref={setMenuRef}
                      button={<Ionicons name='md-more' size={35} color="#fff" style={{marginRight:20}} onPress={() =>showMenu()} />}
                    >
                      <MenuItem onPress={()=>{navigation.navigate('PerfilUsaurio'); hideMenu()}}>{'Perfil'}</MenuItem>
                      <MenuItem onPress={()=>EventRegister.emit('logOut','logOut')}>{'Cerrar sesión'}</MenuItem>

                    </Menu>
                    </TouchableOpacity>,
                    }),
  },
  EventoCreate: {
    screen: EventoCreateScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Crear Evento',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
    }),
  },

  EventoPropuestaCreate: {
    screen: PropuestaScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Crear Propuesta',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
    }),
  },

  EventoDetail: {
    screen: EventoDetailScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Evento',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
    }),
  },


});
const _menu = {};
const setMenuRef = ref => {
  _menu = ref;
};

const hideMenu = () => {
  _menu.hide();
};

const showMenu = () => {
  _menu.show();
};
const EventosStackCliente = createStackNavigator({
  Eventos: {
    screen: EventosScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Mis Eventos',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
      headerRight: (
        <View style={{flexDirection:'row'}}>
         <Ionicons name='md-add' size={35} color="#fff" style={{marginRight:20}} onPress={() => navigation.navigate('EventoCreate')}/>
         <TouchableOpacity onPress={() =>showMenu()}>
          <Menu
              ref={setMenuRef}
              button={<Ionicons name='md-more' size={35} color="#fff" style={{marginRight:20}} onPress={() =>showMenu()} />}
            >
              <MenuItem onPress={()=>{navigation.navigate('PerfilUsaurio'); hideMenu()}}>{'Perfil'}</MenuItem>
              <MenuItem onPress={()=>EventRegister.emit('logOut','logOut')}>{'Cerrar sesión'}</MenuItem>

            </Menu>
            </TouchableOpacity>
        </View>
      ),
    }),
  },
  EventoCreate: {
    screen: EventoCreateScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Crear Evento',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
    }),
  },

   EventoEdit: {
    screen: EventoCreateScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Editar Evento',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
    }),
  },

  EventoPropuestaCreate: {
    screen: PropuestaScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Crear Evento',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
    }),
  },

  EventoDetail: {
    screen: EventoDetailScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Evento',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
      headerRight: (
        <View style={{flexDirection:'row'}}>
         <TouchableOpacity onPress={() => EventRegister.emit("onUpdateEvent")}><Ionicons name='md-create' size={25} color="#fff" style={{marginRight:20}} onPress={() => EventRegister.emit("onUpdateEvent")}/></TouchableOpacity>
         <TouchableOpacity onPress={() => EventRegister.emit("onDeleteEvent")}><Ionicons name='md-trash' size={25} color="#fff" style={{marginRight:20}} onPress={() => EventRegister.emit("onDeleteEvent")}/></TouchableOpacity>
        </View>
      ),
    }),
  },
  DetailPerfil: {
    screen: PerfilScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Perfil',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
    }),
  },
  DetailMensaje: {
    screen: DetailMensajeScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Mensaje',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
    }),
  },


});


const MensajesStack = createStackNavigator({
  Mensajes: {
    screen: MensajesScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Mensajes',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
      headerRight: (
        <TouchableOpacity  onPress={() =>showMenu()} style={{flexDirection:'row'}}>
          <Menu
              ref={setMenuRef}
              button={<Ionicons name='md-more' size={35} color="#fff" style={{marginRight:20}} onPress={() =>showMenu()} />}
            >
              <MenuItem onPress={()=>{navigation.navigate('PerfilUsaurio'); hideMenu()}}>{'Perfil'}</MenuItem>
              <MenuItem onPress={()=>EventRegister.emit('logOut','logOut')}>{'Cerrar sesión'}</MenuItem>

            </Menu>

        </TouchableOpacity>
      ),
    }),
  },

  DetailMensaje: {
    screen: DetailMensajeScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Mensaje',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
    }),
  },


});

const PerfilStack = createStackNavigator({
  DetailPerfil: {
    screen: PerfilScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Perfil',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },

    }),
  }




});

const PerfilContactStack = createStackNavigator({
  DetailPerfil: {
    screen: ContactPerfilScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Perfil',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
      headerLeft: (
        <TouchableOpacity  onPress={() =>navigation.navigate('Tabs')} style={{flexDirection:'row'}}>
          <View style={{flexDirection:'row', marginLeft:20}}>
            <Ionicons name='md-arrow-back'  size={25} color="#fff" onPress={() => navigation.navigate('Tabs')}/>
          </View>
        </TouchableOpacity>
      ),
    }),
  },





});

const ProveedoresStack = createStackNavigator({
  Proveedores: {
    screen: ProveedoresScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Proveedores',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
      headerRight: (
        <TouchableOpacity  onPress={() =>showMenu()} style={{flexDirection:'row'}}>
          <Menu
              ref={setMenuRef}
              button={<Ionicons name='md-more' size={35} color="#fff" style={{marginRight:20}} onPress={() =>showMenu()} />}
            >
              <MenuItem onPress={()=>{navigation.navigate('PerfilUsaurio'); hideMenu()}}>{'Perfil'}</MenuItem>
              <MenuItem onPress={()=>EventRegister.emit('logOut','logOut')}>{'Cerrar sesión'}</MenuItem>

            </Menu>

        </TouchableOpacity>
      ),
    }),

  },

  DetailProveedores: {
    screen: DetailProveedoresScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Proveedores',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
    }),
  },
  DetailPerfil: {
    screen: PerfilScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Perfil',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
    }),
  },
  contactInformation: {
    screen: contactInformationScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Perfil',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
      headerLeft: (
        <View style={{flexDirection:'row', marginLeft:20}}>
          <Ionicons name='md-arrow-back'  size={25} color="#fff" style={{marginLeftt:20}} onPress={() => navigation.goBack()}/>


        </View>
      ),
    }),

  },
  DetailMensaje: {
    screen: DetailMensajeScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Mensaje',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
    }),
  },


});

const HistorialStack = createStackNavigator({
  Historial: {
    screen: HistorialScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Historial',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
      headerRight: (
        <TouchableOpacity  onPress={() =>showMenu()} style={{flexDirection:'row'}}>
          <Menu
              ref={setMenuRef}
              button={<Ionicons name='md-more' size={35} color="#fff" style={{marginRight:20}} onPress={() =>showMenu()} />}
            >
              <MenuItem onPress={()=>{navigation.navigate('PerfilUsaurio'); hideMenu()}}>{'Perfil'}</MenuItem>
              <MenuItem onPress={()=>EventRegister.emit('logOut','logOut')}>{'Cerrar sesión'}</MenuItem>

            </Menu>

        </TouchableOpacity>
      ),
    }),
  },
  EventoDetail: {
    screen: EventoDetailScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Evento',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
    }),
  },
  DetailPerfil: {
    screen: PerfilScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Perfil',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
    }),
  },
  DetailMensaje: {
    screen: DetailMensajeScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Mensaje',
      headerTintColor: '#fff',
      headerStyle: { backgroundColor: '#ea1d75' },
    }),
  },
});

export const TabsProveedor = createBottomTabNavigator(
  {
    Eventos: EventosStackProveedor,
    Mensajes: MensajesStack,
    Perfil: PerfilStack,
    Historial: HistorialStack
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === "Eventos") {
          iconName = `ios-calendar${focused ? "" : "-outline"}`;
        } else if (routeName === "Mensajes") {
          iconName = `ios-chatboxes${focused ? "" : "-outline"}`;
        } else if (routeName === "Perfil") {
          iconName = `ios-person${focused ? "" : "-outline"}`;
        } else if (routeName === "Historial") {
          iconName = `ios-clock${focused ? "" : "-outline"}`;
        }

        // You can return any component that you like here! We usually use an
        // icon component from react-native-vector-icons
        return <Ionicons name={iconName} size={25} color={tintColor} />;
      }
    }),
    tabBarOptions: {
      activeTintColor: "#ea1d75",
      inactiveTintColor: "gray",
      style: {
        backgroundColor: '#fff',
        height: 60,
      },
      tabStyle : {

      },
      labelStyle : {
        marginBottom:10
      }
    }
  }
);

export const TabsCliente = createBottomTabNavigator(
  {
    Eventos: EventosStackCliente,
    Mensajes: MensajesStack,
    Proveedores: ProveedoresStack,
    Historial: HistorialStack,

  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === "Eventos") {
          iconName = `ios-calendar${focused ? "" : "-outline"}`;
        } else if (routeName === "Mensajes") {
          iconName = `ios-chatboxes${focused ? "" : "-outline"}`;
        } else if (routeName === "Proveedores") {
          iconName = `ios-person${focused ? "" : "-outline"}`;
        } else if (routeName === "Historial") {
          iconName = `ios-clock${focused ? "" : "-outline"}`;
        }

        // You can return any component that you like here! We usually use an
        // icon component from react-native-vector-icons
        return <Ionicons name={iconName} size={25} color={tintColor} />;
      }
    }),
    tabBarOptions: {
      activeTintColor: "#ea1d75",
      inactiveTintColor: "gray",
      style: {
        backgroundColor: '#fff',
        height: 60,
      },
      tabStyle : {

      },
      labelStyle : {
        marginBottom:10
      }
    }
  }
);


export const RootProveedor = createStackNavigator(
  {


    Tabs: {
      screen: TabsProveedor,
    },
    PerfilUsaurio:{
      screen:PerfilContactStack
    }
  } ,
  {
    mode: "modal",
    headerMode: "none",

  }
);

export const RootCliente = createStackNavigator(
  {


    Tabs: {
      screen: TabsCliente,
    },
    PerfilUsaurio:{
      screen:PerfilContactStack
    }
  } ,
  {
    mode: "modal",
    headerMode: "none",

  }
);
