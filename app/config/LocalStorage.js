import { AsyncStorage } from 'react-native';

const getUser = async () => {
	let user = await AsyncStorage.getItem('user');
	try {
	    user =  JSON.parse(user);
	    return user;
	} catch (e) {
	    return null;
	}
} 

const setUser = async (data) => {
	console.log('entre aqui')
	try {
		if (data) {
			await AsyncStorage.setItem('user', JSON.stringify(data));
		} else {
			await AsyncStorage.setItem('user', '');
		}
		console.log('entre aqui')
		
	} catch (error){
		console.log('error', error)
	}
}

export default {
	getUser,
	setUser
};