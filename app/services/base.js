
export default class base {
	constructor(props) {
	  
	  	this.url = props;
	  	this.geturi = this.geturi.bind(this);
	  	this.get = this.get.bind(this);
	  	this.geturl = this.geturi.bind(this);
	  	this.post = this.post.bind(this);
	  	this.headers = {
	  		'Content-type':'application/json',
	  		'Accept':'application/json',
	  	}
		
	}

	get = (uri, params) => {

		return fetch(this.geturi(uri, params), {
			method:'GET',
			header:this.headers
		}).then(resp=>resp.json());
    
	}

	postImage = (uri, data) => {
		let params = new FormData();
		Object.keys(data).map(key=>{
			console.log(key, data[key])
			params.append(key, data[key]);
		})
		return fetch(this.geturi(uri,{}), {
			method:'POST',
			body:params
		}).then(resp=>resp.json());

	}

	post = (uri, params) => {
		
		return fetch(this.geturi(uri,params), {
			method:'POST',
			header:this.headers,
			body:JSON.stringify(params)
		}).then(resp=>resp.json());

	}

	put = (uri, params) => {
		
		return fetch(this.geturi(uri,params), {
			method:'PUT',
			header:this.headers,
			body:JSON.stringify(params)
		}).then(resp=>{console.log(resp.status) ;return resp.json()});

	}

	delete = (uri, params) => {
		
		return fetch(this.geturi(uri,params), {
			method:'DELETE',
			header:this.headers,
			body:JSON.stringify(params)
		}).then(resp=>resp.json());

	}

	geturi = (uri, params) =>{
		let url = `${this.url}/${uri}`
	
    const query = Object.keys(params).map(k => {
        if (Array.isArray(params[k])) {
        	if (!params[k].length) 
        		return `${encodeURIComponent(k)}[]=`
            return params[k]
                .map(val => `${encodeURIComponent(k)}[]=${encodeURIComponent(val)}`)
                .join('&')
        }
        return `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`
    }).join('&')
    if (query) {
        url = `${url}?${query}`
    }

    console.log('URL', url.toString())
    return url.toString()
	}

	geturl = (url, params) =>{
        const query = Object.keys(params).map(k => {
            if (Array.isArray(params[k])) {
                return params[k]
                    .map(val => `${encodeURIComponent(k)}[]=${encodeURIComponent(val)}`)
                    .join('&')
            }
            return `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`
        }).join('&')
        if (query) {
            url = `${url}?${query}`
        }

        console.log('URL', url.toString())
        return url.toString()
	}

}