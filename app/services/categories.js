import Base from './base';

class categories extends Base {

	createproposal = (data) => {
		return this.post('new-category', data);
	}

	getcategoriesForId = (id) => {
		return this.get('categories',{id});
	}

	getCategories = () => {
		return this.get('get-categories', {});
	}

	mycategories = (token) => {
		return this.get("my-categories", {token});
	}

	categorieEvent = categories_id => {
		return this.get(`get-events-by-category`, {categories_id});
	}

	profileEvent = categories_id => {
		return this.get(`get-profiles-by-category`, {categories_id});
	}
	
	getEvents = ()=>{
	  return this.get("get-events", {});
	}	
	
		
}

export default new categories('http://198.211.114.51/api')