import Base from './base';

class Proposals extends Base {

	createproposal = (data) => {
		return this.post('new-proposal', data);
	}

	getproposalsForId = (id) => {
		return this.get('proposals',{id});
	}

	myproposals = (token) => {
		return this.get("my-proposals", token);
	}

	proposalEvent = (idEven) => {
		return this.get(`event/${idEven}/proposals`,{});
	}
	
	getproposalsQuery = (query)=>{
	  return this.get("proposals", query);
	}

	updateStatus = (status, user_id, event_id, rating)=>{
		return this.put('update-proposal-status', {status, user_id, event_id, rating});
	}
	

	sendNotifications = (data)=>{
		return this.post('sendNotifications', data);
	}

	getHistory = token => (
		this.get('get-proposals',{token})
	)

	
		
}

export default new Proposals('http://198.211.114.51/api')