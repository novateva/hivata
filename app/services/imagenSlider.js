import Base from './base';

class User extends Base {

	Create = (image, user_id)=>{
		return this.postImage('upload-image', { image, user_id });
	}

	Delete = (image_id, user_id)=>{
		return this.post('delete-image', {image_id, user_id});
	}

	ALL = (id)=>{
		return this.get(`image-user/${id}`, {});
	}

	Update = (image, image_id)=>{
		return this.postImage('update-image', {image, image_id})
	}

	UploadPerfil = (image, user_id)=>{
		return this.postImage('upload-image-perfil', { image, user_id });
	}
}

export default new User('http://198.211.114.51/api')
