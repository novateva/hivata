import Base from './base';

class Events extends Base {

	createEvent = (data) => {
		return this.post('new-event', data);
	}

	Update = (data, id) => {
		return this.put('update-event/' + id, data);
	}

	Delete = (id) => {
		return this.delete('delete-event/' + id, {});
	}

	getEventsForId = (id) => {
		return this.get('events',{id});
	}

	getEventId = (id) => {
		return this.get("events/"+id,{});
	}
	myEvents = (token) => {
		return this.get("my-events", {token});
	}
	
	getEvents = (token)=>{
	  return this.get("get-events", {token});
	}	

	getEventsHistory = (token) => {
		return this.get('my-history',{token});
	}	
	
	
}

export default new Events('http://198.211.114.51/api')