import Base from './base';

class User extends Base {

	login = (email, password)=>{
		return this.post('login', { email, password });
	}

	register = (data)=>{
		return this.post('register', data);
	}

	me = (token) => (
		this.get('me',{token} )
	)

	refresh = (token)=>{
		return this.post('refresh', {token});
	}

	finishWork = (id, rating)=>{
		return this.post('finishWork/'+id, {rating})
	}

	updateUser = (token,data) => {
		return this.put('me', {token, ...data});
	}
	GetUser = (token, id) => {
		return this.get('user/' + id,{token});
	}
	SendPost = data =>{
		return this.post('sendpost', data);
	}
	recovery = data => {
		return this.post('password/email', data);
	}
	exitsUser = data => {
		return this.get('user/email', data);
	}
	getSuscriptionUrl = (code) => {
		return this.get('suscribed', {code});
	}
	makeTrasaction = (token, iduser, code, buget) =>{
	  return this.post('created-transaccion',{token, iduser, code, buget})
	}
	
}

export default new User('http://198.211.114.51/api')