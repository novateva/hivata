import React from 'react';
import { Text, View, StyleSheet, ScrollView,  ActivityIndicator, Image, TouchableOpacity, RefreshControl } from 'react-native';
import Proposal from '../services/proposal';
import Event from '../services/events';




export default class HistorialScreen extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {
      proposal:[],
      activity:false
    };
    
    /*
    Firebase.database().ref(`chat/${this.props.screenProps.user.user.id}`)
      .on('value', (dataSnapshot) => { 
            const data = dataSnapshot.val();
            `console`.log(data)
            this.setState({proposal:data})
      })*/
      this.getData = this.getData.bind(this)

      
  }


  /*
     Object.keys(this.state.proposal).map((key)=>(
          this.state.proposal[key].proposal.status > 1 ?
          <View style={styles.evento}>
            <Text style={styles.eventoTitulo}>{this.state.proposal[key].event.name}</Text>
            <View style={styles.eventoInfo}>
              <Text style={styles.eventoPrecio}>{"$" +  this.state.proposal[key].proposal.budget}</Text>
              <Text style={styles.eventoFecha}>{this.state.proposal[key].event.date.split(' ')[0]}</Text>
            </View>
            <Text style={styles.eventoUsuario}>{this.state.proposal[key].proposal.users.profile.name}</Text>
          </View> 
          :null
  */


  componentWillMount() {
    
    this.getData();
  }
  getData() {
    this.setState({activity:false})
    if(this.props.screenProps.user.user.roles_id === 1) {
       Proposal.getHistory(this.props.screenProps.user.token)
        .then((data)=>{
         
          this.setState({activity:true,proposal:data ? data : []})
        })
    } else {
      Event.getEventsHistory(this.props.screenProps.user.token)
        .then((data)=>{
          
          if(!data)return;
          const aux = [];
          for (var i = 0; i < data.length; i++) {
            if(!data[i].proposals) continue;
             
            for (var j = data[i].proposals.length - 1; j >= 0; j--) {
              if(data[i].proposals[j].status > 1) {
                aux.push({events:data[i], proposal:data[i].proposals[j]})
                break;
              }
            }
          }
          this.setState({activity:true,proposal:aux });
        })
    }
  }
 
  render() {
     
    return (
     this.state.activity ?
      <ScrollView refreshControl={
        <RefreshControl
          refreshing={false}
          onRefresh={this.getData}
        />
        } style={styles.contenedor}>
        {
          !this.state.proposal.length ? 
          <View style={{width:"100%", height:'100%', flexDirection:'column', justifyContent:'center', alignItems: 'center'}}>
            <Image
              style={styles.imagen}
              source={require('../img/triste.png')}
            />
            <Text style={{textAlign:'center', fontSize:20, fontWeight:'bold'}} >{'¡Aún no hay historial!'}</Text>
            <Text style={{textAlign:'center'}} >{`¡Oh no! Este lugar esta vacío ${this.props.screenProps.user.user.roles_id === 2 ? 'crea nuevos eventos' : 'envía nuevas propuestas'} y crea un historial!`}</Text>
              
          </View>
          :
          this.props.screenProps.user.user.roles_id === 1 ? 
          this.state.proposal.map(data=>{
            if(data.status < 2) return null;
            return (
              <TouchableOpacity onPress={()=>this.props.navigation.navigate('EventoDetail', {event:data.events, fromHistorial:true})} style={styles.evento}>
                <Text style={styles.eventoTitulo}>{data.events.name}</Text>
                <View style={styles.eventoInfo}>
                  <Text style={styles.eventoPrecio}>{"$" +  data.budget}</Text>
                  <Text style={styles.eventoFecha}>{data.events.date.split(' ')[0]}</Text>
                </View>
                <Text style={styles.eventoUsuario}>{data.users.profile.name}</Text>
              </TouchableOpacity>
            );
          })
          :this.state.proposal.map(data=>{
            return (
              <TouchableOpacity onPress={()=>this.props.navigation.navigate('EventoDetail', {event:data.events, fromHistorial:true})} style={styles.evento}>
                <Text style={styles.eventoTitulo}>{data.events.name}</Text>
                <View style={styles.eventoInfo}>
                  <Text style={styles.eventoPrecio}>{"$" +  data.proposal.budget}</Text>
                  <Text style={styles.eventoFecha}>{data.events.date.split(' ')[0]}</Text>
                </View>
                <Text style={styles.eventoUsuario}>{data.proposal.users.profile.name}</Text>
              </TouchableOpacity>
            );
          })
        
      }
      </ScrollView>
      :<View style={[styles.contenedor, {marginTop:'48%'}]}>
      <ActivityIndicator size="large" color="#ea1d75" />
    </View>
  
    );
  }
}

const styles = StyleSheet.create({
  contenedor : {
    flex: 1,
    
  },

  evento: {
    padding: 20,
    borderBottomColor: "#ecf0f1",
    borderBottomWidth: 1,
    backgroundColor: "#fff",
    
  },

  eventoTitulo : {
    fontSize: 17,
    marginBottom: 5,
    fontWeight: 'normal',
    color: "#DEB749"
  },

  eventoInfo : {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  eventoPrecio : {
    fontWeight: 'bold',
  },
  eventoFecha : {

  },
  eventoUsuario : {
    marginTop: 10,
  },
  imagen:{
    width:30, 
    height:30, 
    marginTop:'48%'
  }



});


