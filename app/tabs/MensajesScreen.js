import React from 'react';
import { Modal, Text, View, StyleSheet, Image, ScrollView,BackHandler, TouchableOpacity, ActivityIndicator } from 'react-native';
import Ionicons from "react-native-vector-icons/Ionicons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Firebase from '../config/firebase';
import ERROR from '../config/Error';

export default class MensajesScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      starCount: 3.5,
      chat:{},
      activity:false,
      deleteEvent:false,
      imagenUser : this.props.screenProps.user.user.roles_id !== 1 ? require('../img/proveedor.png') : require('../img/cliente.png')
    };
    this.deleteChat = this.deleteChat.bind(this)
    this.ref = Firebase.database().ref(`chat/${this.props.screenProps.user.user.id}`)
        .on('value', (dataSnapshot) => {

            this.setState({chat: dataSnapshot.val() ? dataSnapshot.val() : [], activity:true})

        });

    this.refa = Firebase.database().ref(`chat/${3}`)
        .on('value', (dataSnapshot) => {

            console.log(dataSnapshot)

        });
    


  }




  componentWillUnmount() {
    Firebase.database().ref(`chat/${this.props.screenProps.user.user.id}`).off('value', this.ref)
  }

  onDeleteEvent(key) {
        this.setState({deleteEvent:true, keyDelete:key})
  }

  deleteChat() {
    try {
      this.setState({loadingDelete:true})
      Firebase.database().ref(`chat/${this.props.screenProps.user.user.id}/${this.state.keyDelete}`).set(null)
      Firebase.database().ref(`chat/${this.state.chat[this.state.keyDelete].proposal.users.id}/${this.state.keyDelete}`).set(null)

      ERROR("Chat eliminado con éxito","éxito");
      this.setState({loadingDelete:false, deleteEvent:false})
    
    } catch(error) {
      console.log(error)
    }
  }

  render() {
    console.log(this.state.chat)
    return (
      this.state.activity ?

      <ScrollView style={styles.container}>
        {
          !Object.keys(this.state.chat).length ?
          <View style={{width:"80%",marginLeft:'10%', height:'100%', flexDirection:'column', justifyContent:'center', alignItems: 'center'}}>
            <Image
              style={styles.imagen}
              source={require('../img/triste.png')}
            />
            <Text style={{textAlign:'center', fontSize:20, fontWeight:'bold'}} >{'¡Aún no tienes chats!'}</Text>
            <Text style={{textAlign:'center'}} >{`¡Oh no! Este lugar esta vacío ${this.props.screenProps.user.user.roles_id === 2 ? 'crea nuevos eventos' : 'envia nuevas propuestas'} y comienza a chatear con tus ${this.props.screenProps.user.user.roles_id === 2 ? 'proveedores' : 'clientes'}!`}</Text>

          </View>
          :
          Object.keys(this.state.chat).map((idEvent,i)=>(
           ( this.state.chat[idEvent].proposal &&  this.state.chat[idEvent].proposal.users) ?
            <TouchableOpacity style={styles.mensaje} onPress={() => this.props.navigation.navigate('DetailMensaje', {idEvent})}>
              <View style={styles.mensajeWrapAvatar}>
                <Image style={styles.mensajeAvatar} source={this.state.chat[idEvent].proposal.users.imagenPath === '' ? this.state.imagenUser:{uri:'http://198.211.114.51/api/retrieve-image/'+this.state.chat[idEvent].proposal.users.imagenPath}}/> 
              </View>

              <View style={styles.mensajeInfo}>
                <Text style={styles.mensajeNombre}>{this.state.chat[idEvent].proposal.users.profile.name}</Text>
                <Text style={styles.mensajeNombreProyecto}>{this.state.chat[idEvent].event.name}</Text>
              </View>

              <Ionicons name={'ios-arrow-forward'} size={25} color={"#DEB749"} />
              <TouchableOpacity onPress={()=>this.onDeleteEvent(idEvent)}>
                <Ionicons name='md-trash' size={25} color="red" style={{paddingLeft:10, paddingRight:10}} />
              </TouchableOpacity>

            </TouchableOpacity>
            : null
          ))
        }
        <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.deleteEvent}

            onRequestClose={() => {
                this.setState({paypalModal:false})
            }}>

            <View style={{backgroundColor:'rgba(0,0,0,0.6)', width:'100%', height:'100%',justifyContent:'center'}}>
            
                <View style={[styles.modal, {height:150}]}>
                     <TouchableOpacity
                            style={{position:'absolute', top:5,left:5}}
                            onPress={()=>this.setState({deleteEvent:false,})}
                            >
                        <MaterialIcons size={30} name='clear' />
                    </TouchableOpacity>
                    <Text style={{textAlign:'center', marginTop:20, marginBottom:15}}>{`¿Estas seguro que quieres eliminar el chat con ${this.state.chat[this.state.keyDelete] ? this.state.chat[this.state.keyDelete].proposal.users.profile.name:''}?`}</Text>
                    <TouchableOpacity
                        style={styles.buttonModal}
                        onPress={this.deleteChat}
                        >
                        {
                        this.state.loadingDelete ?
                            <ActivityIndicator style={{marginTop:10}} color="#fff"/>
                        :
                            <Text style={[styles.buttonTextModal]}>{"Eliminar"}</Text>
                        }

                    </TouchableOpacity>

                </View>
            </View>
        </Modal>
      </ScrollView>
      :
      <View style={[styles.contenedor, {marginTop:'48%'}]}>
        <ActivityIndicator size="large" color="#ea1d75" />
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container : {
    flex: 1,
  },

  mensaje : {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 5,
    padding: 20,
    height: 90,
    borderBottomColor: "#ecf0f1",
    borderBottomWidth: 1,
  },
  buttonTextModal : {
      color: "#fff",
      textAlign:'center',
      fontSize: 17,
      paddingVertical:10
  },
  buttonModal : {
      height: 50,
      backgroundColor:"#ea1d75",
      width:'80%',
      alignSelf: 'stretch',

      marginLeft:'10%',
      borderRadius: 3,
  },
  modal:{
      backgroundColor:'#eee',
      width:'70%',
      marginLeft:'15%',
      height:250,
      borderColor:'black',
      borderWidth:1,
      borderRadius:10,
      justifyContent:'center'

  },
  modalText:{
      color: "#000",
      textAlign:'center',
      fontSize: 17,
  },

  paypalLogo:{
      width:30,
      height:30
  },

  mensajeWrapAvatar : {
    borderRadius: 100,
    width: 60,
    height: 60,
    marginRight: 20,
  },
  mensajeAvatar : {
    width: 60,
    height: 60,
    marginBottom: 50,
    resizeMode: 'contain',
    borderRadius: 30,

  },

  mensajeInfo : {

    height: 60,
    width: "55%",

  },

  mensajeNombre : {
    fontSize: 17,
    marginBottom:5,
  },

  mensajeNombreProyecto : {
    color: "gray",
    fontSize: 15,
  },
  imagen:{
    width:30,
    height:30,
    marginTop:'48%'
  }


});
