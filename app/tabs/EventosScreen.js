import React from 'react';
import { Text, View, StyleSheet, ScrollView, BackHandler, TouchableOpacity, RefreshControl, Image, ActivityIndicator } from 'react-native';
import events from '../services/events';
import { EventRegister } from 'react-native-event-listeners';
import { Notifications } from 'expo';

export default class EventosScreen extends React.Component {


  constructor(props) {
    super(props);
  
    this.state = {
      events:[],
      activity:false
    };
    this.getData = this.getData.bind(this);
    this.getData();
    this.goToEvent = this.goToEvent.bind(this);
    this._handleNotification = this._handleNotification.bind(this);
    
  }
  componentDidMount() {
      EventRegister.addEventListener('eventCreated',() => { 
        this.getData();
      });
      this._notificationSubscription = Notifications.addListener(this._handleNotification);
     
     
 
     
  }

  
  

  _handleNotification = (notification) => {
     
     if (notification.origin === 'selected') {

        this.props.navigation.navigate('DetailMensaje', {idEvent:notification.data.events_id})
      
     }
     
  };
  

  getData(){
    this.setState({refreshing:true})
    if(this.props.screenProps.user.user.roles_id === 1) {
      events.getEvents(this.props.screenProps.user.token)
      .then(data=>this.setState({refreshing:false, activity:true ,events: data ? data.reverse() : []}))
    } else {
      events.myEvents(this.props.screenProps.user.token)
      .then(data=>this.setState({refreshing:false, activity:true ,events: data ? data.reverse() : []}))
    }
    
  }

  goToEvent(event) {
    if(this.props.screenProps.user.user.roles_id === 2) {
      this.props.navigation.navigate('EventoDetail', {event:event})
    } else {
      this.props.navigation.navigate('EventoPropuestaCreate', {event:event})
    }
    
  }

  render() {
    
    return (
      <ScrollView
        refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this.getData}
        />
        }
      >
      {this.state.activity ?
        <View style={styles.contenedor}>
        {
          !this.state.events.length 
          ?
          <View style={{width:"100%", height:'100%', flexDirection:'column', justifyContent:'center', alignItems: 'center'}}>
            <Image
              style={styles.imagen}
              source={require('../img/triste.png')}
            />
            <Text style={{textAlign:'center', fontSize:20, fontWeight:'bold'}} >{'¡Aún no tienes eventos!'}</Text>
            <Text style={{textAlign:'center'}} >{this.props.screenProps.user.user.roles_id === 2 ? '¡Oh no! Este lugar esta vacío crea tu primer evento y crece rápido!' : '¡Oh no! Este lugar esta vacío espera que clientes publiquen nuevos eventos!'}</Text>
            {this.props.screenProps.user.user.roles_id === 2 &&  <TouchableOpacity 
                style={styles.button}
                onPress={()=>this.props.navigation.navigate('EventoCreate')}>
                {
    
                  <Text style={styles.buttonText}>{'Nuevo'}</Text>  
                }
              </TouchableOpacity>}
              
          </View>
          : 
          this.state.events.map((data,i)=>{
            return (
              <TouchableOpacity key={i} style={data.send ? [styles.evento, {borderWidth:2, borderColor:'#DEB749', borderStyle:'solid'}] : styles.evento} onPress={()=>this.goToEvent(data)}>                
                <Text style={styles.eventoTitulo}>{data.name}</Text>
                <Text style={styles.eventoDescripcion}>{data.description}</Text>
                <Text style={styles.eventoFecha}>{data.date.split(" ")[0]}</Text>
                <Text style={styles.eventoTitulo}>{data.status > 0 ? (data.status < 2 ? "En curso" : "Finalizada") : `Propuestas: ${data.proposals_count}`}</Text>
              </TouchableOpacity>
            );
          })
        }
        </View> 
        :
        <View style={[styles.contenedor, {marginTop:'48%'}]}>
          <ActivityIndicator size="large" color="#ea1d75" />
        </View>
      } 
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  contenedor : {
    flex: 1,
    padding: 20,
  },
  evento: {
    backgroundColor: "#fff",
    padding: 10,
    paddingRight:15,
    borderRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: {width:0, height:0},
    shadowColor: 'gray',
    shadowRadius: 10,
    marginBottom:20,
    // borderBottomColor: "gray",
    // borderBottomWidth: 2,
  },
  buttonText : {
    color: "#fff",
    textAlign:'center',
    fontSize: 17,
  },
  button : {
    height: 50,
    backgroundColor:"#ea1d75",
    // backgroundColor:"#feda73",
    alignSelf: 'stretch',
    paddingVertical: 15,
    borderRadius: 3,
    marginTop:'3%',
    width:'70%',
    marginLeft:'15%'
  },
  eventoTitulo : {
    fontSize: 17,
    marginBottom: 5,
    color: "#DEB749"
  },

  eventoDescripcion : {
    color: "#212121"
  },

  eventoFecha : {
    marginTop: 10,
    color: "#212121"
  },

  imagen:{
    width:30, 
    height:30, 
    marginTop:'48%'
  }


});


