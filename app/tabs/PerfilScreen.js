import React from 'react';
import { ActivityIndicator, Text, View, StyleSheet, ScrollView, TextInput, RefreshControl, TouchableOpacity, Image, Modal } from 'react-native';
import Ionicons from "react-native-vector-icons/Ionicons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import StarRating from 'react-native-star-rating';
import RadioButton from 'react-native-radio-button'
import Categories from '../services/categories';
import User from '../services/user';
import ERROR from '../config/Error';
import Slider from '../components/Slider/Slideshow';
import expo, { ImageManipulator } from 'expo';
import ImagenSlider from '../services/imagenSlider';
import Select from 'react-native-picker-select';






export default class PerfilScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      starCount: 3.5,
      userId: this.props.screenProps.user.user.id,
      text:'',
      categories:[],
      user:{},
      profile:{},
      categoriasSelected:[],
      profileToSend:{},
      Images:[],
      isModal:false,
      indexImagenEdit:-1,
      seeUser: this.props.navigation.state.params !== undefined,
      position:0,
      interval:null
    };
    
    
    

    this.submitValue = this.submitValue.bind(this);
    this.addImage = this.addImage.bind(this);
    this.DeleteImage = this.DeleteImage.bind(this);
    this.editImage = this.editImage.bind(this);
    this.getData = this.getData.bind(this);
    
  }

  componentWillMount() {
    this.seeUser =  this.props.navigation.state.params !== undefined;
    this.getData();
    


  }
  
  componentWillReceiveProps(props) {
    console.log('props',props)
  }
  
  getDerivedStateFromProps(props) {
    console.log('props',props)
  }

  getData() { 
    this.setState({refreshing:true});
    if (!this.state.seeUser) {
        User.me(this.props.screenProps.user.token)
            .then(dataUser=>{
                
                this.setState({user:dataUser.user,profile:dataUser.profile, });
                Categories.getCategories()
                    .then(data=>{
                        const filterCategories = data.map(data=>data.id);
                        const filterUserCategory = dataUser.profile.categories.map(data=>data.id);
                       
                        //console.log(data)
                        
                        let categoriasSelected = [];
                        filterCategories.forEach((idCategories,i) => {
                            if(filterUserCategory.indexOf(idCategories) !== -1){
                                categoriasSelected[i] = true;
                            }
                        });
                        this.setState({categories:data ? data : [], categoriasSelected, refreshing:false})
                    })
            })
        ImagenSlider.ALL(this.props.screenProps.user.user.id)
            .then(data=>{
                this.setState({Images:data ? data : []})
            })
    } else {
        User.GetUser(this.props.screenProps.user.token,this.props.navigation.state.params.user.id)
            .then(dataUser=>{
                this.setState({user:dataUser.user,profile:dataUser.profile, });

                Categories.getCategories()
                    .then(data=>{
                        const filterCategories = data.map(data=>data.id);
                        const filterUserCategory = dataUser.profile.categories.map(data=>data.id);
                        
                        
                        let categoriasSelected = [];
                        filterCategories.forEach((idCategories,i) => {
                            if(filterUserCategory.indexOf(idCategories) !== -1){
                                categoriasSelected[i] = true;
                            }
                        });
                        this.setState({categories:data ? data : [], categoriasSelected, refreshing:false})
                    })
            })
        ImagenSlider.ALL(this.props.navigation.state.params.user.id)
            .then(data=>{
                this.setState({Images:data ? data : []})

            })    
    }

  }

  submitValue(){
    
    
    const categoriasSelected = []
    Object.keys(this.state.categoriasSelected).forEach(key=>{
        if(this.state.categoriasSelected[key])
            categoriasSelected.push(this.state.categories[key].id);
    });
    
    this.state.profile.categoriasSelected = categoriasSelected;
    
    if((this.state.profile.contactEmail !== '' && this.state.profile.contactEmail !== 'null' && this.state.profile.contactEmail !== null) && !(/^\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/).test(this.state.profile.contactEmail)) {

        ERROR("Formato incorrecto de correo","Error");
        return;
    }

    if((this.state.profile.website !== '' && this.state.profile.website !== 'null' && this.state.profile.website !== null)  && !(/[a-z0-9\.-]+\.[a-z]{2,4}/gi).test(this.state.profile.website)) {

        ERROR("Formato incorrecto de web","Error");
        return;
    }
    
    
    this.setState({loading:true})
    console.log(this.state.profile)
    User.updateUser(this.props.screenProps.user.token,this.state.profile).then((data)=>{
        
      ERROR("Perfil Actualizado","Éxito");
      this.setState({loading:false})
    })
    .catch((error)=>{
        ERROR("Error actualizando perfil","Error");
        this.setState({loading:false})
    });   
   
  }

  async addImage() {
    
    expo.ImagePicker.launchImageLibraryAsync({
        mediaTypes:'Images',
        allowsEditing:true,
        width: 400,
        quality:0.5,
        aspect:[6,3],
        height: 100,
        cropping: true
    })
        .then(result => {
    
            if(!result.cancelled){
                this.setState({loadingAddImage:true})
                ImagenSlider.Create({ uri: result.uri, type: `${result.type}/${result.uri.substr(result.uri.lastIndexOf('.')+1)}`, name:('imagenPerfil' + String(this.state.Images.length)) }, this.props.screenProps.user.user.id )
                    .then(data=>{
                        
                        this.state.Images.push(data.image)
                        this.setState({Images:this.state.Images, loadingAddImage:false})
                        ERROR("Imagen Agregada","Éxito");
                    })
                    .catch(erro=>{});
            }
        })

  }

  onChangeValue(name, value) {
    this.setState({profile:{
        ...this.state.profile,
      [name] : value
    }})
  }

  componentWillUnmount() {
    clearInterval(this.state.interval);
  }

  async editImage() {
    if (this.state.indexImagenEdit === -1) {
         ERROR("Selecciona una imagen!","Error");
         return;
    }
    const result = await  expo.ImagePicker.launchImageLibraryAsync({
        mediaTypes:'Images',
        allowsEditing:true,
        width: 400,
        aspect:[6,3],
        quality:0.5,
        height: 100,
        cropping: true
    })
    if(!result.cancelled){
        this.setState({loadingEditImage:true})
        ImagenSlider.Update({ uri: result.uri, type: `${result.type}/${result.uri.substr(result.uri.lastIndexOf('.')+1)}`, name:('imagenPerfil' + String(this.state.indexImagenEdit)) },this.state.Images[this.state.indexImagenEdit].id)
            .then(data=>{
                
                this.state.Images[this.state.indexImagenEdit] = data.image
                this.setState({Images:this.state.Images, isModal:false, loadingEditImage:false, indexImagenEdit:-1});
                ERROR("Imagen Editada","Éxito");
            })
    }
  }

  DeleteImage() {
    if (this.state.indexImagenEdit === -1) {
         ERROR("Selecciona una imagen!","Error");
         return;
    }
    this.setState({loadingDeleteImage:true})
    ImagenSlider.Delete(this.state.Images[this.state.indexImagenEdit].id, this.props.screenProps.user.user.id)
    .then(data=>{
        this.setState({loadingDeleteImage:false,Images:this.state.Images.filter((data, i)=> i !== this.state.indexImagenEdit ), indexImagenEdit:-1})   
        ERROR("Imagen eliminada","Éxito");
    }) 
  }

  render() {
   
    
    return (
        <ScrollView refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.getData}
            />
            } style={styles.container}>
        
            <View style={{'flexDirection':'row'}}>
                <View style={{'flexDirection':'column', 'flex':0.6}}>
                    <Text style={[styles.label,{marginLeft:5}]} >
                        {"Nombre Compañía"}
                    </Text>
                    {
                        !this.state.seeUser ? <TextInput 
                        editable={!this.state.seeUser}
                        style={styles.input}
                        ref={component => this.name = component}
                        onSubmitEditing={()=>this.description.focus()}
                        placeholder={this.state.profile.name !== 'null' ? this.state.profile.name: ''}
                        value={this.state.profile.name!== 'null' ?  this.state.profile.name : ''}
                        placeholderTextColor='#ccc'
                        underlineColorAndroid={'rgba(0,0,0,0)'}
                        onChangeText={(text)=>this.onChangeValue('name', text)}
                        >
                    </TextInput> :  <Text style={[styles.input, {paddingTop:3, height:'auto', minHeight:50}]}>{this.state.profile.name !== "null" ? this.state.profile.name : ""}</Text>
                    }
                    
                </View>
                <View style={{'flexDirection':'column', 'flex':0.4, marginLeft:'2%'}}>
                    <Text style={styles.label} >
                        {"Calificación"}
                    </Text>
                    <View style={styles.proveedorAction}>
                        <StarRating
                            disabled={true}
                            maxStars={5}
                            rating={this.state.profile.rating/this.state.profile.finishWork}
                            fullStarColor={'#f1c40f'}
                            starSize={12}
                            containerStyle={{width: 80, marginTop:10}}
                        />
                    </View>

                </View>
            </View>


        <Text style={styles.label}>{'Descripción'}</Text>
        {
            !this.state.seeUser ? <TextInput 
          editable={!this.state.seeUser}
          style={styles.textarea}
          ref={component => this.description = component}
          placeholder={this.state.profile.description !== 'null' ? this.state.profile.description: ''}
          placeholderTextColor= '#ccc'
          multiline={true}
          value={this.state.profile.description!== 'null' ?  this.state.profile.description : ''}
          numberOfLines={10}
          onChangeText={(text)=>this.onChangeValue('description', text)}
          underlineColorAndroid={'rgba(0,0,0,0)'}>
          
        </TextInput>:  <Text style={[styles.input, {paddingTop:3, height:'auto', minHeight:50}]}>{this.state.profile.description !== "null" ? this.state.profile.description : ""}</Text>
        }
        

        

        {this.state.categories.map((data, i)=>(
            i%2 === 0 ?
            <View style={{'flex':1, 'flexDirection':'row'}}>
                <View style={{'flex':0.5, 'flexDirection':'row', marginLeft:'2%', 'marginTop':'2%', 'alignItems':'center'}}>
                    <RadioButton
                        animation={'bounceIn'}
                        innerColor={'#ea1d75'}
                        outerColor={'#ea1d75'}
                        isSelected={this.state.categoriasSelected[i]}
                        onPress={this.state.seeUser ? null : () => this.setState({categoriasSelected:{...this.state.categoriasSelected, [i]:!this.state.categoriasSelected[i]}})}
                    />
                    <TouchableOpacity onPress={this.state.seeUser ? null : () => this.setState({categoriasSelected:{...this.state.categoriasSelected, [i]:!this.state.categoriasSelected[i]}})}>
                        <Text style={[styles.label, {marginLeft:'2%', paddingRight:20}]}>{data.name}</Text>
                    </TouchableOpacity>
                   
               </View>
               { this.state.categories[i+1] ?
                    <View style={{'flex':0.5, 'flexDirection':'row', marginLeft:'2%', 'marginTop':'2%', 'alignItems':'center'}}>
                       <RadioButton
                            animation={'bounceIn'}
                            innerColor={'#ea1d75'}
                            outerColor={'#ea1d75'}
                            isSelected={this.state.categoriasSelected[i+1]}
                            onPress={this.state.seeUser ? null : () => this.setState({categoriasSelected:{...this.state.categoriasSelected, [i+1]:!this.state.categoriasSelected[i+1]}})}
                        />
                        <TouchableOpacity onPress={this.state.seeUser ? null : () => this.setState({categoriasSelected:{...this.state.categoriasSelected, [i+1]:!this.state.categoriasSelected[i+1]}})}>
                            <Text style={[styles.label, {marginLeft:'2%', paddingRight:20}]}>{this.state.categories[i+1].name}</Text>
                        </TouchableOpacity>
                       
                       
                   </View>
                :null}
            </View>
            : null
             ))}
        
        {this.state.categories.length %2 !== 0 ?
            <View style={{'flexDirection':'row', marginLeft:'2%', 'marginTop':'2%', 'alignItems':'center'}}>
                <RadioButton
                    animation={'bounceIn'}
                    innerColor={'#ea1d75'}
                    outerColor={'#ea1d75'}
                    isSelected={this.state.categoriasSelected[this.state.categories.length - 1]}
                    onPress={() => this.setState({categoriasSelected:{...this.state.categoriasSelected, [this.state.categories.length - 1]:!this.state.categoriasSelected[this.state.categories.length - 1]}})}
                />
               <Text style={[styles.label,{paddingRight:20}]}>{this.state.categories[this.state.categories.length -1].name}</Text>
            </View>
            : null
        }
        <View style={{height:'20%', position:'relative', marginTop:'2%'}}>
            <Slider
                position={this.state.position}
                onPositionChanged={position => this.setState({ position })} 
                height={'100%'}
               
                dataSource={this.state.Images.map(data=>({url:'http://198.211.114.51/api/retrieve-image/'+data.path}))}
                
            
            />
            {(this.state.Images.length > 0 && !this.state.seeUser)  && <TouchableOpacity 
                style={[styles.buttonPlusSlider,this.state.Images.length === 5 ? {paddingVertical:5} : {right:37, paddingVertical:5}]}
                onPress={()=>this.setState({isModal:true})}>
                {
                    <Ionicons style={styles.buttonText} name='md-create'/>   
                }
            </TouchableOpacity>}
            {(this.state.Images.length < 4 && !this.state.seeUser) && <TouchableOpacity 
                style={styles.buttonPlusSlider}
                onPress={this.addImage}>
                {
                    
                    this.state.loadingAddImage ?
                    <ActivityIndicator color="#fff"/>
                    :
                    <Text style={styles.buttonText}>+</Text>    
                    
                    
                }
            </TouchableOpacity>}
            <Modal
                animationType="slide"
                transparent={true}
                
                visible={this.state.isModal}
                
                onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                <View style={{backgroundColor:'rgba(0,0,0,0.6)', width:'100%', height:'100%', justifyContent:'center'}}>
                    
                    <View style={styles.modal}>
                        <TouchableOpacity 
                                style={{position:'absolute', top:5,left:5}}
                                onPress={()=>this.setState({isModal:false})}
                                >
                            <MaterialIcons size={30} name='clear' />
                        </TouchableOpacity>
                        <View style={{flex:0.6, alignItems:'center'}}>
                            <Text style={[styles.modalText]}>{"Edita tu imágenes"}</Text> 
                            <View style={{backgroundColor:'rgba(255, 255, 255, 0.3)', marginBottom:15, width:'80%'}}>
                                <Select
                                    placeholder={{
                                        label: 'Selecciona una imagen...',
                                        value: -1,
                                    }}
                                    items={this.state.Images.map((data, i)=>({label:`Imagen ${i+1}`, value:i, color:'black'}))}
                                    onValueChange={(value) => this.setState({indexImagenEdit:value})}
                                    value={this.state.indexImagenEdit}
                                />
                            </View>
                            {this.state.indexImagenEdit !== -1 && <Image
                                style={[styles.paypalLogo,{marginTop:'7%'}]}
                                source={{ uri: 'http://198.211.114.51/api/retrieve-image/' + this.state.Images[this.state.indexImagenEdit].path }}
                            />}
                        
                         
                        </View>
                        <View style={{flex:0.1, flexDirection:'row'}}>
                            <TouchableOpacity 
                                style={styles.buttonModal}
                                onPress={()=>this.DeleteImage()}
                                >
                                {
                                    this.state.loadingDeleteImage ?
                                    <ActivityIndicator style={{marginTop:10}} color="#fff"/>
                                    :
                                    <Text style={styles.buttonTextModal}>{"Eliminar"}</Text>  
                                }
                                  
                                    
                            </TouchableOpacity>
                            <TouchableOpacity 
                                style={styles.buttonModal}
                                onPress={()=>this.editImage()}
                                >
                                {
                                    this.state.loadingEditImage ?
                                    <ActivityIndicator style={{marginTop:10}} color="#fff"/>
                                    :
                                    <Text style={styles.buttonTextModal}>{"Cambiar"}</Text>  
                                }

                                    
                            </TouchableOpacity>
                        </View>
                            
                    </View>
                    
                </View>
            </Modal>
        </View>
        <Text style={styles.label}>{'Página web'}</Text>
        {
            !this.state.seeUser ?<TextInput 
          editable={!this.state.seeUser}
          ref={component => this.website = component}
          onSubmitEditing={()=>this.contactName.focus()}
          style={styles.input}
          placeholder={this.state.profile.website !== 'null' ? this.state.profile.website: ''}
          value={this.state.profile.website!== 'null' ?  this.state.profile.website : ''}
          placeholderTextColor='#ccc'
          onChangeText={(text)=>this.onChangeValue('website', text)}
          underlineColorAndroid={'rgba(0,0,0,0)'}
          >
        </TextInput> :  <Text style={[styles.input, {paddingTop:3, height:'auto', minHeight:50}]}>{this.state.profile.website !== "null" ? this.state.profile.website : ""}</Text>
        }
        
        
        <Text style={[styles.label,{marginLeft:5, marginTop:'2%'}]} >
            {"Persona de contacto"}
        </Text>
        {
            !this.state.seeUser ?<TextInput 
          editable={!this.state.seeUser}
            style={styles.input}
            ref={component => this.contactName = component}
            onSubmitEditing={()=>this.contactEmail.focus()}
            placeholder={this.state.profile.contactName !== 'null' ? this.state.profile.contactName: ''}
            placeholderTextColor='#ccc'
            value={this.state.profile.contactName!== 'null' ?  this.state.profile.contactName : ''}
            underlineColorAndroid={'rgba(0,0,0,0)'}
            onChangeText={(text)=>this.onChangeValue('contactName', text)}
            >
        </TextInput> :  <Text style={[styles.input, {paddingTop:3, height:'auto', minHeight:50}]}>{this.state.profile.contactName !== "null" ? this.state.profile.contactName : ""}</Text>
        }
        

        <Text style={[styles.label,{marginLeft:5}]} >
            {"Email"}
        </Text>
        {
            !this.state.seeUser ? <TextInput 
            editable={!this.state.seeUser}
            style={styles.input}
            ref={component => this.contactEmail = component}
            onSubmitEditing={()=>this.phone.focus()}
            placeholder={this.state.profile.contactEmail !== 'null' ? this.state.profile.contactEmail: ''}
            value={this.state.profile.contactEmail!== 'null' ?  this.state.profile.contactEmail : ''}
            placeholderTextColor='#ccc'
            underlineColorAndroid={'rgba(0,0,0,0)'}
            onChangeText={(text)=>this.onChangeValue('contactEmail', text)}
            >
        </TextInput> :  <Text style={[styles.input, {paddingTop:3, height:'auto', minHeight:50}]}>{this.state.profile.contactEmail !== "null" ? this.state.profile.contactEmail : ""}</Text>
        }
        

        <Text style={[styles.label,{marginLeft:5}]} >
            {"Teléfono"}
        </Text>
        {
            !this.state.seeUser ?<TextInput 
            editable={!this.state.seeUser}
            style={styles.input}
            ref={component => this.phone = component}
            onSubmitEditing={()=>this.cellPhone.focus()}
            placeholder={this.state.profile.phone !== 'null' ? this.state.profile.phone: ''}
            value={this.state.profile.phone!== 'null' ?  this.state.profile.phone : ''}
            placeholderTextColor='#ccc'
            keyboardType='numeric'
            underlineColorAndroid={'rgba(0,0,0,0)'}
            onChangeText={(text)=>this.onChangeValue('phone', text)}
            >
        </TextInput>
         :  <Text style={[styles.input, {paddingTop:3, height:'auto', minHeight:50}]}>{this.state.profile.phone !== "null" ? this.state.profile.phone : ""}</Text>
        }
        
        <Text style={[styles.label,{marginLeft:5}]} >
            {"Celular"}
        </Text>
        {
            !this.state.seeUser ?<TextInput 
            editable={!this.state.seeUser}
            style={styles.input}
            ref={component => this.cellPhone = component}
            placeholder={this.state.profile.cellPhone !== 'null' ? this.state.profile.cellPhone: ''}
            value={this.state.profile.cellPhone!== 'null' ?  this.state.profile.cellPhone : ''}
            placeholderTextColor='#ccc'
            keyboardType='numeric'
            underlineColorAndroid={'rgba(0,0,0,0)'}
            onChangeText={(text)=>this.onChangeValue('cellPhone', text)}
            >
        </TextInput> :  <Text style={[styles.input, {paddingTop:3, height:'auto', minHeight:50}]}>{this.state.profile.cellPhone !== "null" ? this.state.profile.cellPhone : ""}</Text>
        }
        
        
        {!this.state.seeUser ? 
            <TouchableOpacity 
            style={styles.button}
            onPress={this.submitValue}>
            {
                this.state.loading ?
                <ActivityIndicator style={{marginTop:6}} color="#fff"/>
                :
                <Text style={styles.buttonText}>Actualizar</Text>  
            }
            </TouchableOpacity>
            :
            <TouchableOpacity 
                style={styles.button}
                onPress={()=>{if (this.state.refreshing) {return;} this.props.navigation.navigate('contactInformation',{user:this.props.navigation.state.params.user, profile:this.state.profile} )}}>
                {
                
                <Text style={styles.buttonText}>{'Enviar mensaje'}</Text>  
                }
            </TouchableOpacity>
        }


        <View style={{backgroundColor:'transparent', height:300}} />
          
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
     container : {
        
        paddingBottom: 40,
        marginLeft:'5%',
        width:'90%',
        marginTop:20
    },

    label : {
      fontSize: 15,
      color: "#666",
      marginBottom: 3
    },
    input: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        borderRadius: 3,
    },

    picker: {
      backgroundColor:'rgba(255, 255, 255, 1)',
      height: 50,
      width:'100%',
      color:"#212121",
      alignSelf: 'stretch',
      padding:10
    },

    textarea: {
        padding: 10,
        backgroundColor:'rgba(255, 255, 255, 1)',
        height: 50,
        alignSelf: 'stretch',
        color:"#212121",
        fontSize: 17,
        marginBottom: 20,
        height: 100,
        textAlignVertical: "top",
        borderRadius: 3,
    },

    contentText: { color: '#fff' },

    button : {
        height: 50,
        backgroundColor:"#ea1d75",
        // backgroundColor:"#feda73",
        alignSelf: 'stretch',
        paddingVertical: 15,
        borderRadius: 3,
        marginTop:'3%'
    },

    buttonPlusSlider : {
        height: 30,
        backgroundColor:"#ea1d75",
        // backgroundColor:"#feda73",
        alignSelf: 'stretch',
        paddingVertical: 3,
        width:30,
        borderRadius:250,
        position:'absolute',
        top: 5,
        right: 5
    },
    


    buttonText : {
        color: "#fff",
        textAlign:'center',
        fontSize: 17,
    },
    proveedorAction : {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    customSlide: {

        borderColor:'black',
        borderWidth:1,
        backgroundColor: '#eee',
        alignItems: 'center',
        justifyContent: 'center',
       
        
    },
    customImage: {
        width: 100,
        height: 100,
    },
    buttons: {
        zIndex: 1,
        height: 15,
        marginTop: -25,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    buttonSlider: {
        margin: 3,
        width: 15,
        height: 15,
        opacity: 0.9,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonTextModal : {
        color: "#fff",
        textAlign:'center',
        fontSize: 14,
        paddingVertical:10
    },
    buttonModal : {
        height: 40,
        backgroundColor:"#ea1d75",
        width:'30%',
        alignSelf: 'stretch',
        marginLeft:'12.5%',
        borderRadius: 5,
    },
    modal:{
        backgroundColor:'#eee',
        width:'70%',
        marginLeft:'15%',
        height:300,
        borderColor:'black',
        borderWidth:1,
        borderRadius:10,
        justifyContent:'center'

    },
    modalText:{
        color: "#000",
        textAlign:'center',
        fontSize: 17,
    },

    paypalLogo:{
        width:100,
        height:60
    }

});


