import React, { Component } from 'react';
import { RootProveedor, RootCliente, AuthStack } from './config/router';
import LocalStorage from './config/LocalStorage';
import User from './services/user';
import { Permissions, Notifications } from 'expo';
import { EventRegister } from 'react-native-event-listeners';
import {  View, Image, Linking} from 'react-native';


class App extends Component {
	constructor(props) {
	  super(props);

	  this.state = {
	  	user:null,
	  };

	  this.logIn = this.logIn.bind(this);
	  this.logOut = this.logOut.bind(this);
	  this.registerForPushNotificationsAsync = this.registerForPushNotificationsAsync.bind(this);
	}
	componentWillMount(){
		this.calculateUser();
		EventRegister.addEventListener('logOut',this.logOut);
		


	}
	async registerForPushNotificationsAsync() {
		const { status: existingStatus } = await Permissions.getAsync(
			Permissions.NOTIFICATIONS
		);
		let finalStatus = existingStatus;

		if (existingStatus !== 'granted') {
			const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
			finalStatus = status;
		}

		if (finalStatus !== 'granted') {
			return;
		}

		let tokenNotification = await Notifications.getExpoPushTokenAsync();

		User.updateUser(this.state.user.token, {tokenNotification}).then(data=>{console.log('notification',data)})

	}


	async calculateUser() {
		this.state.user = await LocalStorage.getUser();

		if(!this.state.user){
			this.setState({loading:true});
			return;
		}
		User.login(this.state.user.user.email,this.state.user.user.password)
			.then(data => {
				console.log(data)
				if (data.error) {
					this.setState({loading:true, user:null});
					return;
				}
				User.me(data.access_token)
	              .then(dataUser=>{

					
	                this.setState({loading:true ,user:{
						 	user:{
								...this.state.user.user,
								...dataUser.user
							},
							profile:{
								...this.state.user.profile,
								...dataUser.profile
							},
							token:data.access_token
						}
					}, ()=>{
						this.registerForPushNotificationsAsync()
					})


	            })


			})


	}

	logIn(user) {
		LocalStorage.setUser(user);
		this.setState({ user });
	}

	logOut() {
		LocalStorage.setUser(null);
		this.setState({ user:null });

	}

	render() {
		
		if(!this.state.loading)
			return <View style={{alignItems:'center', justifyContent:'center', height:'100%'}}>
						<Image
				          style={{width:'100%', height:'100%'}}
				          source={require('./img/splash.png')}
				        />
			       </View>

		if(!this.state.user){
			return <AuthStack screenProps={{logIn:this.logIn}}/>
		} else {
			if (this.state.user.user.roles_id == 1) {
				return <RootProveedor screenProps={{user:this.state.user, logOut:this.logOut}}/>;
			} else {
				return <RootCliente screenProps={{user:this.state.user, logOut:this.logOut}}/>;
			}
		}

	}
}

export default App;
